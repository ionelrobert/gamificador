<?php

namespace App\Console\Commands;

use App\Alumno;
use App\Mail\AlumnoEmail;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Support\Facades\Mail;

class AvisoAlumno extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'aviso:alumno';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Aviso Alumno, Quitar vida';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $fecha = Carbon::now();
        if(!$fecha->isWeekend()){
            $alumnos = Alumno::all();
            $fecha = $fecha->format('Y-m-d');
            foreach ($alumnos as $alumno){
                if($alumno->ultimo_login < $fecha){
                    if($alumno->salud != 0){
                        $salud = $alumno->salud - 1;
                        $alumno->salud = $salud;
                        $alumno->save();
                    }
                    Mail::to($alumno->user()->first()->email)->send(new AlumnoEmail($alumno));
                }
            }
        }
    }
}
