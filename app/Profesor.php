<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profesor extends Model
{
    protected $table = "profesores";
    protected $primaryKey = 'user_id';
    protected $fillable = [
        'nombre', 'apellidos'
    ];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function modulos()
    {
        return $this->hasMany(Modulo::class, 'profesor_id');
    }

    /**
     * @return string
     */
    public function getFullNameAttribute(){
        return $this->attributes['nombre'].' '.$this->attributes['apellidos'];
    }
}
