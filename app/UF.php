<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UF extends Model
{
    protected $table = "ufs";
    protected $primaryKey = 'id';
    protected $fillable = [
        'nombre','horas','modulo_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function alumnos()
    {
        return $this->belongsToMany(Alumno::class,'uf_alumno','uf_id','alumno_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function modulo()
    {
        return $this->belongsTo(Modulo::class, 'modulo_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function nfs()
    {
        return $this->hasMany(NF::class, 'uf_id');
    }

    /**
     * @return int
     */
    public function tieneItemsActivos(){
        $activos = 0;
        foreach ($this->nfs()->get() as $nf){
            foreach ($nf->items()->get() as $item){
                if($item->activo){
                    $activos += 1;
                }
            }
        }
        return $activos;
    }

    /**
     * @return array
     */
    public function ranking(){
        $alumnos = array();
        $nota = 0;
        //dd($this->id);
        foreach ($this->nfs()->get() as $nf){
            foreach ($nf->items()->get() as $item){
                foreach ($item->alumnos()->get() as $alumno){
                    $alumnos  [$alumno->user_id] = array('nota' => $alumno->notaUF($this), 'nombre'=>$alumno->nombre.' '.$alumno->apellidos);
                }
            }
        }
        $alumnos = collect($alumnos)->sortBy('nota')->reverse()->toArray();
        return $alumnos;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function buscarAlumno($id){
        return Alumno::find($id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function randomAlumno($id){
        $alumnos = [];
        foreach ($this->alumnos()->get() as $alumno){
            if($alumno->user_id != $id){
                $alumnos [] = $alumno;
            }
        }
        return $alumnos[array_rand($alumnos)];
    }
}
