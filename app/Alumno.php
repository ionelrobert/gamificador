<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alumno extends Model
{
    protected $table = "alumnos";
    protected $primaryKey = 'user_id';
    protected $fillable = [
        'nombre', 'apellidos','salud','energia'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function cursos()
    {
        return $this->belongsToMany(Curso::class, 'alumno_curso', 'alumno_id', 'curso_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function items()
    {
        return $this->belongsToMany(Item::class,'alumno_item','alumno_id','item_id')->withPivot('valoracion','nota');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function ufs()
    {
        return $this->belongsToMany(UF::class,'uf_alumno','alumno_id','uf_id');
    }

    /**
     * @return string
     */
    public function getFullNameAttribute(){
        return $this->attributes['nombre'].' '.$this->attributes['apellidos'];
    }

    /**
     * @param UF $uf
     * @return int
     */
    public function notaUF(UF $uf){
        $media = 0;
        foreach ($uf->nfs()->get() as $nf){
            foreach ($nf->items()->get() as $item){
                foreach ($item->alumnos()->get() as $alumno){
                    if(($alumno->pivot->alumno_id == $this->attributes['user_id'])
                        && ($item->id == $alumno->pivot->item_id)){
                        $media += $alumno->pivot->nota;
                    }
                }
            }
        }
        return $media;
    }

    /**
     * @param UF $uf
     * @return int
     */
    public function valoracionItems(UF $uf){
        $val = 0;
        foreach ($uf->nfs()->get() as $nf){
            foreach ($nf->items()->get() as $item){
                foreach ($item->alumnos()->get() as $alumno){
                    if(($alumno->pivot->alumno_id == $this->attributes['user_id'])
                        && ($item->id == $alumno->pivot->item_id)){
                        //$val += $alumno->pivot->valoracion;
                        $val += $alumno->pivot->nota;
                    }
                }
            }
        }
        if($val<20){
            $val =  20;
        }elseif($val>100){
            $val =  100;
        }
        return $val;
    }

    /**
     * @param UF $uf
     * @return int
     */
    public function logros(UF $uf){
        $val = 0;
        foreach ($uf->nfs()->get() as $nf){
            foreach ($nf->items()->get() as $item){
                foreach ($item->alumnos()->get() as $alumno){
                    if(($alumno->pivot->alumno_id == $this->attributes['user_id'])
                        && ($item->id == $alumno->pivot->item_id)){
                        //$val += $alumno->pivot->valoracion;
                        $val += $alumno->pivot->nota;
                    }
                }
            }
        }
        if($val >= 0 && $val<=25){
            return 1;
        }elseif($val >= 25 && $val <= 50){
            return 2;
        }elseif($val >= 50 && $val <= 75){
            return 3;
        }
        return 4;
    }

    /**
     * @param $nombre
     * @return int
     */
    public function heroe($nombre){
        if(\Auth::user()->alumno()->first()->personaje == $nombre){
            return 1;
        }
    }
}
