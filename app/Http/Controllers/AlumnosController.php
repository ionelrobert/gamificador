<?php

namespace App\Http\Controllers;

use App\Alumno;
use App\Mail\AlumnoEmail;
use App\UF;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rule;
use DateTimeZone;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Jenssegers\Agent\Agent;

class AlumnosController extends Controller
{
    /**
     * AlumnosController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){
        $alumnos = Alumno::all();
        return view('auth.templates-profesores.alumnos.index', compact('alumnos'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function dashboardAlumnos(){
        $modulos = [];
        $mod = 0;
        if(\Auth::user()->alumno()->get()->first() != null){
            if(count(\Auth::user()->alumno()->first()->ufs()->get())>0){
                foreach (\Auth::user()->alumno()->first()->ufs()->get() as $uf){
                    if($mod != $uf->modulo()->first()['id']){
                        $modulos  [] = $uf->modulo()->first();
                        $mod = $uf->modulo()->first()['id'];
                    }
                }
            }
        }
        return view('auth.templates-alumnos.dashboard', compact('modulos'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function detallesUf($id){
        $uf = UF::find($id);
        $items = [];
        if($uf == null){
            abort(403);
        }
        if($this->tieneLaUf($uf) == false){
            abort(404);
        }else{
            if(count(\Auth::user()->alumno()->first()->items()->get())>0){
                foreach (\Auth::user()->alumno()->first()->items()->get() as $item){
                    if($item->nf()->first()->uf()->first()->id == $uf->id){
                        $items [] = $item;
                    }
                }
            }
        }
        return view('auth.templates-alumnos.uf', compact('uf', 'items'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function perfil(){
        $usuario = \Auth::user();
        return view('auth.templates-alumnos.perfil', compact('usuario'));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function editarPerfil(Request $request){
        $this->validate($request, [
            'nombre' => ['required'],
            'email' => ['required','email',Rule::unique('users')->ignore($request->id)],
            'apellidos' => ['required'],
            'password' => 'min:0|confirmed',
            'personaje' => ['required']
        ]);
        $usuario = User::find($request->id);
        $usuario->name = $request->nombre;
        $usuario->email = $request->email;

        if($request->password){
            $usuario->password = bcrypt($request->password);
        }

        if($request->file('imagen')){

            $path = public_path('/perfiles/alumnos/');

            $img = Image::make($request->file('imagen'));

            $imageName =  $request->email.'-'.$request->file('imagen')->getClientOriginalName();

            $img->fit(400, 400);

            if(file_exists(public_path('/perfiles/alumnos/'.$usuario->imagen))) {
                \File::delete(public_path('/perfiles/alumnos/'.$usuario->imagen));
            }
            $img->save(public_path('/perfiles/alumnos/'.$imageName));

            $usuario->imagen = $imageName;
        }

        $alumno = Alumno::find($request->id);

        if(!$alumno){
            $alumno = new Alumno();
            $alumno->nombre = $request->nombre;
            $alumno->apellidos = $request->apellidos;
            $alumno->user_id = $request->id;
            $alumno->personaje = $request->personaje;
        }else{
            $alumno->nombre = $request->nombre;
            $alumno->apellidos = $request->apellidos;
            $alumno->personaje = $request->personaje;
        }
        if($usuario->save() && $alumno->save()){
            return redirect('/alumnos/miperfil')->withSuccess("Perfil editado correctamente.");
        }else{
            return redirect('/alumnos/miperfil')->withWarning("Error al editar tú perfil correctamente.");
        }
    }

    /**
     * @param $uf
     * @return bool
     */
    public function tieneLaUf($uf){
        $esta = false;
        foreach ($uf->alumnos()->get() as $alumno){
            if($alumno->user_id == \Auth::user()->alumno()->first()->user_id){
                $esta = true;
            }
        }
        return $esta;
    }
}
