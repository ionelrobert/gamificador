<?php

namespace App\Http\Controllers;

use App\Profesor;
use App\User;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
//use Intervention\Image\Image;
use Intervention\Image\Facades\Image;

class UserProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function indexProfesor(){
        $usuario = \Auth::user();
        return view('auth.templates-profesores.perfil.perfil', compact('usuario'));
    }

    public function editPerfilProfesor(Request $request){
        //dd($request->file('imagen'));
        $this->validate($request, [
            'nombre' => ['required'],
            'email' => ['required','email',Rule::unique('users')->ignore($request->id)],
            'apellidos' => ['required'],
            'password' => 'min:0|confirmed',
        ]);

        $usuario = User::find($request->id);
        $usuario->name = $request->nombre;
        $usuario->email = $request->email;

        if($request->password){
            $usuario->password = bcrypt($request->password);
        }

        if($request->file('imagen')){

            $path = public_path('/perfiles/profesores/');

            $img = Image::make($request->file('imagen'));

            $imageName =  $request->email.'-'.$request->file('imagen')->getClientOriginalName();

            $img->fit(400, 400);

            if(file_exists(public_path('/perfiles/profesores/'.$usuario->imagen))) {
                \File::delete(public_path('/perfiles/profesores/'.$usuario->imagen));
            }
            $img->save(public_path('/perfiles/profesores/'.$imageName));

            $usuario->imagen = $imageName;
        }

        $profesor = Profesor::find($request->id);

        if(!$profesor){
            $profesor = new Profesor();
            $profesor->nombre = $request->nombre;
            $profesor->apellidos = $request->apellidos;
            $profesor->user_id = $request->id;
        }else{
            $profesor->nombre = $request->nombre;
            $profesor->apellidos = $request->apellidos;
        }
        if($usuario->save() && $profesor->save()){
            return redirect('/admin/mi-perfil/')->withSuccess("Perfil editado correctamente.");
        }else{
            return redirect('/admin/mi-perfil/')->withSuccess("Error al editar tú perfil correctamente.");
        }
    }
}
