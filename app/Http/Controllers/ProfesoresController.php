<?php

namespace App\Http\Controllers;

use App\Alumno;
use App\Item;
use App\Modulo;
use App\NF;
use App\Profesor;
use App\UF;
use Illuminate\Http\Request;

class ProfesoresController extends Controller
{
    /**
     * ProfesoresController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){
        $profesores = Profesor::all();
        return view('auth.templates-profesores.profesores.index', compact('profesores'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function modulos(){
        return view('auth.templates-profesores.profesores.modulos');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function ufs(){
        return view('auth.templates-profesores.profesores.ufs');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function nfs(){
        return view('auth.templates-profesores.profesores.nfs');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function create(Request $request){
        $this->validate($request, [
            'nombre' => ['required'],
            'peso' => ['required','integer','max:100','min:1'],
            'uf_id' => ['required','integer'],
        ]);
        $nf = new NF();
        $nf->fill($request->all());
        $nf->modulo_id = UF::find($request->uf_id)->modulo->id;
        if($nf->save()){
            return redirect('/admin/profe/nfs')->withSuccess("Nuclio Formativo añadido correctamente.");
        }else{
            return redirect('/admin/profe/nfs')->withWarning("Error al añadir el nf, vuelva a intentarlo en unos minutos.");
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function edit(Request $request){
        $this->validate($request, [
            'nombre' => ['required'],
            'peso' => ['required','integer','max:100','min:1'],
            'uf_id' => ['required','integer'],
        ]);
        $nf = NF::find($request->id);
        $nf->nombre = $request->nombre;
        $nf->peso = $request->peso;
        $nf->uf_id = $request->uf_id;
        $nf->modulo_id = UF::find($request->uf_id)->modulo->id;
        if($nf->save()){
            return redirect('/admin/profe/nfs')->withSuccess("Nuclio Formativo editado correctamente.");
        }else{
            return redirect('/admin/profe/nfs')->withWarning("Error al añadir el nf, vuelva a intentarlo en unos minutos.");
        }

    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteNfs($id){
        if(NF::destroy($id)){
            return redirect('/admin/profe/nfs')->withSuccess("Nuclio Formativo eliminado correctamente.");
        }else{
            return redirect('/admin/profe/nfs')->withWarning("Error al eliminar el Nuclio Formativo, vuelva a intentarlo en unos minutos.");
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function items(){
        return view('auth.templates-profesores.profesores.items');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function createItem(Request $request){
        $this->validate($request, [
            'nombre' => ['required'],
            'valorMaximo' => ['required','integer','max:100','min:0'],
            'nf_id' => ['required','integer'],
            'examen' => ['required'],
            'activo' => ['required'],
        ]);
        $item = new Item();
        $item->fill($request->all());
        if($item->save()){
            return redirect('/admin/profe/items')->withSuccess("Item, añadido correctamente.");
        }else{
            return redirect('/admin/profe/items')->withWarning("Error al añadir el item, vuelva a intentarlo en unos minutos.");
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function editItem(Request $request){
        $this->validate($request, [
            'nombre' => ['required'],
            'valorMaximo' => ['required','integer','max:100','min:0'],
            'nf_id' => ['required','integer'],
            'examen' => ['required'],
            'activo' => ['required'],
        ]);
        $item = Item::find($request->id);
        $item->fill($request->all());
        if($item->save()){
            return redirect('/admin/profe/items')->withSuccess("Item editado correctamente.");
        }else{
            return redirect('/admin/profe/items')->withWarning("Error al editar el item, vuelva a intentarlo en unos minutos.");
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteItems($id){
        if(Item::destroy($id)){
            return redirect('/admin/profe/items')->withSuccess("Item eliminado correctamente.");
        }else{
            return redirect('/admin/profe/items')->withWarning("Error al eliminar el Item, vuelva a intentarlo en unos minutos.");
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function buscarAlumno(Request $request){
        $this->validate($request, [
            'uf_id' =>  ['required'],
            'alumno_id' => ['required','integer'],
        ]);
        if(!Alumno::find($request->alumno_id)){
            return redirect()->back()->withWarning("El alumno no existe");
        }else{
            $alumno = Alumno::find($request->alumno_id);
            $uf = UF::find($request->uf_id);
            return view('auth.templates-profesores.profesores.calificar',compact('alumno','uf'));
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function puntuarAlumno(Request $request){
        $this->validate($request, [
            'item_id' =>  ['required'],
            'alumno_id' => ['required','integer'],
            'valoracion' => ['required','integer']
        ]);

        if(!Alumno::find($request->alumno_id)){
            return redirect()->back()->withWarning("El alumno no existe");
        }
        $item = Item::find($request->item_id);

        $nota = $request->valoracion * 10 / $item->valorMaximo;
        $item->alumnos()->detach();
        $item->alumnos()->attach([
            $request->alumno_id => ['valoracion' => $request->valoracion,'nota'=>$nota],
        ]);
        if($item->save()){
            return redirect('/admin/profe/ufs')->withSuccess("Nota añadida");
        }else{
            return redirect('/admin/profe/ufs')->withWarning("Fallo al añadir la nota");
        }
    }
    public function deleteItem($id){
        if(Item::destroy($id)){
            return redirect('/admin/profe/items')->withSuccess("Item eliminado correctamente.");
        }else{
            return redirect('/admin/profe/items')->withWarning("Error al eliminar el Item, vuelva a intentarlo en unos minutos.");
        }
    }

}
