<?php

namespace App\Http\Controllers;

use App\Permission;
use Illuminate\Http\Request;

class PermissionsController extends Controller
{
    /**
     * PermissionsController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){
        $permisos = Permission::all();
        return view('auth.templates-profesores.permisos.index', compact('permisos'));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function create(Request $request){
        $this->validate($request, [
            'name' => ['required','unique:permissions'],
            'display_name' => ['required','max:100'],
            'description' => ['required'],
        ]);
        $permiso = new Permission();
        $permiso->fill($request->all());
        if($permiso->save()){
            return redirect('/admin/permisos')->withSuccess("Permiso añadido correctamente.");
        }else{
            return redirect('/admin/permisos')->withWarning("Error al añadir el permiso, vuelva a intentarlo en unos minutos.");
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function edit(Request $request){
        $this->validate($request, [
            'name' => ['required'],
            'display_name' => ['required','max:100'],
            'description' => ['required'],
        ]);
        $permiso = Permission::find($request->id);
        $permiso->fill($request->all());

        if($permiso->save()){
            return redirect('/admin/roles')->withSuccess("Rol editado correctamente.");
        }else{
            return redirect('/admin/roles')->withWarning("Error al editar el rol, vuelva a intentarlo en unos minutos.");
        }

    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id){
        if(Permission::destroy($id)){
            return redirect('/admin/permisos')->withSuccess("Permiso eliminado correctamente.");
        }else{
            return redirect('/admin/permisos')->withWarning("Error al eliminar el permiso, vuelva a intentarlo en unos minutos.");
        }

    }
}
