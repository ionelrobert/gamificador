<?php

namespace App\Http\Controllers\Auth;

use App\Alumno;
use App\Http\Controllers\Controller;
use DateTimeZone;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function redirectTo()
    {
        if(count(Auth::user()->roles()->get()) == 0){
            return '/home';
        }else{
            if(isSuperprofesor() || isProfesor()){
                return '/admin/dashboard/';
            }else{
                $alumno = Alumno::find(\Auth::user()->id);
                $salud = $alumno->salud;
                $fecha = Carbon::now(new DateTimeZone('Europe/Madrid'));
                if(\Auth::user()->alumno->ultimo_login == null){
                    $fecha =$fecha->format('Y-m-d');
                    if($alumno->salud != 100){
                        $alumno->ultimo_login = $fecha;
                        $alumno->salud = $salud + 1;
                        $alumno->save();
                    }
                }else{
                    if(\Auth::user()->alumno->ultimo_login != $fecha->format('Y-m-d')){
                        $fecha =$fecha->format('Y-m-d');
                        if($alumno->salud != 100){
                            $alumno->ultimo_login = $fecha;
                            $alumno->salud = $salud + 1;
                            $alumno->save();
                        }
                    }
                }
                return 'alumnos/dashboard/';
            }
        }
    }
}
