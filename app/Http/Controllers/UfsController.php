<?php

namespace App\Http\Controllers;

use App\Alumno;
use App\Modulo;
use App\UF;
use Illuminate\Http\Request;

class UfsController extends Controller
{
    public function index(){
        return view('auth.templates-profesores.ufs.dashboardufs');
    }

    public function create(Request $request){
        $this->validate($request, [
            'nombre' => ['required'],
            'horas' => ['required','integer'],
            'modulo_id' => ['required'],
            'alumnos'   => ''
        ]);
        //dd($request);
        $uf = new UF();
        $uf->fill($request->all());
        if($uf->save()){
            if($request->modulo_id){
                $uf->modulo()->associate($request->modulo_id);
            }
            if(count($request->alumnos)>0){
                $uf->alumnos()->sync($request->alumnos);
            }
            $uf->save();
            return back()->withSuccess("UF añadida correctamente.");
        }else{
            return back()->withWarning("Error al añadir la UF, vuelva a intentarlo en unos minutos.");
        }
    }

    public function edit(Request $request){
        $this->validate($request, [
            'nombre' => ['required'],
            'horas' => ['required','integer'],
            'modulo_id' => ['required'],
            'alumnos'   => ''
        ]);
        $uf = UF::find($request->id);
        $uf->fill($request->all());

        if($uf->save()){
            if($request->modulo_id){
                $uf->modulo()->associate($request->modulo_id);
            }
            if(count($request->alumnos)>0){
                $uf->alumnos()->sync($request->alumnos);
            }
            $uf->save();
            return  back()->withSuccess("UF editada correctamente.");
        }else{
            return  back()->withWarning("Error al editar la UF, vuelva a intentarlo en unos minutos.");
        }
    }

    public function delete($id){
        if(UF::destroy($id)){
            return back()->withSuccess("UF eliminada correctamente.");
        }else{
            return back()->withWarning("Error al eliminar la uf, vuelva a intentarlo en unos minutos.");
        }

    }

    public function dawUfs(){
        $modulos = Modulo::where('nombre','LIKE','%(CFGS - DAW)%')->get();
        $alumnos = Alumno::all();
        return view('auth.templates-profesores.ufs.ufs',compact('modulos','alumnos'));
    }
    public function asixUfs(){
        $modulos = Modulo::where('nombre','LIKE','%(CFGS - ASIX)%')->get();
        $alumnos = Alumno::all();
        return view('auth.templates-profesores.ufs.ufs',compact('modulos','alumnos'));
    }
    public function smxUfs(){
        $modulos = Modulo::where('nombre','LIKE','%(CFGM - SMX)%')->get();
        $alumnos = Alumno::all();
        return view('auth.templates-profesores.ufs.ufs',compact('modulos','alumnos'));
    }
}
