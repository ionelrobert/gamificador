<?php

namespace App\Http\Controllers;

use App\Alumno;
use App\Curso;
use Illuminate\Http\Request;

class CursosController extends Controller
{
    /**
     * CursosController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){
        $cursos = Curso::all();
        $alumnos = Alumno::all();
        return view('auth.templates-profesores.cursos.index', compact('cursos','alumnos'));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function create(Request $request){
        $this->validate($request, [
            'nombre' => ['required'],
            'grupo' => ['required'],
        ]);
        $curso = new Curso();
        $curso->fill($request->all());
        if($curso->save()){
            $curso->alumnos()->sync($request->alumnos);
            return redirect('/admin/cursos')->withSuccess("Curso añadido correctamente.");
        }else{
            return redirect('/admin/cursos')->withWarning("Error al añadir el curso, vuelva a intentarlo en unos minutos.");
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function edit(Request $request){
        $this->validate($request, [
            'nombre' => ['required'],
            'grupo' => ['required'],
            'alumnos' => ['required'],
        ]);
        $curso = Curso::find($request->id);
        $curso->fill($request->all());

        if($curso->save()){
            $curso->alumnos()->sync($request->alumnos);
            $curso->save();
            return redirect('/admin/cursos')->withSuccess("Curso editado correctamente.");
        }else{
            return redirect('/admin/cursos')->withWarning("Error al editar el curso, vuelva a intentarlo en unos minutos.");
        }

    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id){
        if(Curso::destroy($id)){
            return redirect('/admin/cursos')->withSuccess("Curso eliminado correctamente.");
        }else{
            return redirect('/admin/cursos')->withWarning("Error al eliminar el curso, vuelva a intentarlo en unos minutos.");
        }
    }

    /**
     * @return mixed
     */
    public function reset(){
        $alumnos = Alumno::all();
        $borrado = false;

        if(count($alumnos)>0){
            foreach ($alumnos as $alumno) {
                if($alumno->user()->delete()){
                    $borrado = true;
                }
            }
        }else{
            return redirect('/admin/dashboard')->withWarning("No hay alumnos");
        }

        if($borrado){
            return redirect('/admin/dashboard')->withSuccess("Reset curso realiazdo");
        }else{
            return redirect('/admin/dashboard')->withWarning("Error al hacer el reset del curso");
        }
    }
}
