<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class ConfirmarIdentidadController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){
        return view('auth/confirmacionIdentidad');
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function comprobarIdentidad(Request $request)
   {
       $user = new User();
       $user = $user->findByEmail($request->email)->first();
       $hasher = app('hash');
       if ($hasher->check($request->password, $user->password)) {
           if (isSuperprofesor() || isProfesor()) {
               return redirect('/admin/dashboard/');
           } else {
               return redirect('alumnos/dashboard/');
           }
       } else {
           return redirect()->back()->withErrors(['password' => 'Contraseña incorrecta']);
       }
   }
}
