<?php

namespace App\Http\Controllers;

use App\Alumno;
use App\Profesor;
use App\Role;
use App\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $usuarios = User::all();
        $roles = Role::all();
        return view('auth.templates-profesores.usuarios.index', compact('usuarios','roles'));
    }
    public  function edit(Request $request){
        $profe = false;
        $alumno = false;
        $this->validate($request, [
            'id' => ['required'],
            'roles' => ['required'],
        ]);
        $user = User::find($request->id);

       foreach (Role::find($request->roles) as $rol){
            if($rol->name == 'profe' || $rol->name == 'superprofesor' ){
                $profe = true;
            }elseif ($rol->name == 'alumno'){
                $alumno = true;
            }
       }
       if($user && $request->roles){
            $user->roles()->sync($request->roles);
            if($user->save()){
                $this->buscarProfesor($user->id,$profe,$alumno,$user);
                return redirect('/admin/usuarios')->withSuccess("Asignación de roles al usuario correctamente.");
            }else{
                return redirect('/admin/usuarios')->withWarning("Error al asignar los roles al usuario, vuelva a intentarlo en unos minutos.");
            }
       }else{
           return redirect('/admin/usuarios')->withWarning("Error al asignar los roles al usuario, vuelva a intentarlo en unos minutos.");
       }
    }

    public function buscarProfesor($id,$profe,$alumno, User $user){
        if($profe){
            $profesor = Profesor::find($id);
            if(!$profesor){
                $profesor = new Profesor();
                $profesor->user_id = $user->id;
                $profesor->nombre = $user->name;
                $profesor->save();
            }
        }elseif($alumno){
            $alumne = Alumno::find($id);
            if(!$alumne){
                $alumne = new Alumno();
                $alumne->user_id = $user->id;
                $alumne->nombre = $user->name;
                $alumne->salud = 50;
                $alumne->save();
            }
        }
        return true;
    }
    public function dashboardProfesores(){
        return view('auth.templates-profesores.dashboard');
    }

    public function dashboardAlumnos(){
        return view('auth.templates-alumnos.dashboard');
    }
}
