<?php

namespace App\Http\Controllers;

use App\Modulo;
use App\Profesor;
use Illuminate\Http\Request;

class ModulosController extends Controller
{
    /**
     * ModulosController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){
        $modulos = Modulo::all();
        $profesores = Profesor::all();
        return view('auth.templates-profesores.modulos.index', compact('modulos','profesores'));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function create(Request $request){
        $this->validate($request, [
            'nombre' => ['required'],
            'durada' => ['required','integer'],
            'profesor_id' => ['required'],
        ]);
        $modulo = new Modulo();
        $modulo->fill($request->all());
        if($modulo->save()){
            $modulo->profesor()->associate($request->profesor_id);
            return redirect('/admin/modulos')->withSuccess("Módulo añadido correctamente.");
        }else{
            return redirect('/admin/modulos')->withWarning("Error al añadir el módulo, vuelva a intentarlo en unos minutos.");
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function edit(Request $request){
        $this->validate($request, [
            'nombre' => ['required'],
            'durada' => ['required','integer'],
            'profesor_id' => ['required'],
        ]);
        $modulo = Modulo::find($request->id);
        $modulo->fill($request->all());

        if($modulo->save()){
            if($request->profesor_id){
                $modulo->profesor()->associate($request->profesor_id);
            }
            return redirect('/admin/modulos')->withSuccess("Módulo editado correctamente.");
        }else{
            return redirect('/admin/modulos')->withWarning("Error al editar el módulo, vuelva a intentarlo en unos minutos.");
        }

    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id){
        if(Modulo::destroy($id)){
            return redirect('/admin/modulos')->withSuccess("Módulo eliminado correctamente.");
        }else{
            return redirect('/admin/modulos')->withWarning("Error al eliminar el módulo, vuelva a intentarlo en unos minutos.");
        }

    }
}
