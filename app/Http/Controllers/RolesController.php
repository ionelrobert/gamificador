<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;

class RolesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $roles = Role::all();
        return view('auth.templates-profesores.roles.index', compact('roles'));
    }

    public function create(Request $request){
        $this->validate($request, [
            'name' => ['required','unique:roles'],
            'display_name' => ['required','max:100'],
            'description' => ['required'],
        ]);
        //dd($request);
        $rol = new Role();
        $rol->fill($request->all());

        if($rol->save()){
            return redirect('/admin/roles')->withSuccess("Rol añadido correctamente.");
        }else{
            return redirect('/admin/roles')->withWarning("Error al añadir el rol, vuelva a intentarlo en unos minutos.");
        }

        //return view('auth.templates-profesores.roles.index');
    }
    public function edit(Request $request){
        $this->validate($request, [
            'name' => ['required'],
            'display_name' => ['required','max:100'],
            'description' => ['required'],
        ]);
        $rol = Role::find($request->id);
        $rol->fill($request->all());

        if($rol->save()){
            return redirect('/admin/roles')->withSuccess("Rol editado correctamente.");
        }else{
            return redirect('/admin/roles')->withWarning("Error al editar el rol, vuelva a intentarlo en unos minutos.");
        }

    }
    public function delete($id){
        //$rol = Role::find($id);
        if(Role::destroy($id)){
            return redirect('/admin/roles')->withSuccess("Rol eliminado correctamente.");
        }else{
            return redirect('/admin/roles')->withWarning("Error al eliminar el rol, vuelva a intentarlo en unos minutos.");
        }

    }
}
