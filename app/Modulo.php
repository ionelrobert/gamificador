<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modulo extends Model
{
    protected $table = "modulos";
    protected $primaryKey = 'id';
    protected $fillable = [
        'nombre', 'durada','profesor_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function profesor()
    {
        return $this->belongsTo(Profesor::class, 'profesor_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ufs()
    {
        return $this->hasMany(UF::class, 'modulo_id');
    }

    /**
     * @return array
     */
    public function devolverAlumnosUf(){
        $alumnos = [];
        foreach ($this->ufs()->get() as $uf){
            foreach ($uf->alumnos()->get() as $alumno){
                if(count($alumnos) >0){
                    foreach ($alumnos as $al){
                        if($alumno->user_id != $al->id){
                            $alumnos [] = $alumno;
                        }
                    }
                }else{
                    $alumnos [] = $alumno;
                }
            }

        }
        return $alumnos;
    }
}
