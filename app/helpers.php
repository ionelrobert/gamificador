<?php

/**
 * @return bool
 */
function isSuperprofesor()
{
    return \Auth::user()->roles()->get()->first()->name == 'superprofesor';
}

/**
 * @return bool
 */
function isProfesor()
{
    return \Auth::user()->roles()->get()->first()->name == 'profe';
}

/**
 * @return bool
 */
function isAlumno()
{
    return \Auth::user()->roles()->get()->first()->name == 'alumno';
}

/**
 * @return int
 */
function modulosProfesor(){
    if(count(\Auth::user()->profesor()->get()) != 0){
        return count(\Auth::user()->profesor()->get()->first()->modulos()->get());
    }
    return 0;
}

/**
 * @return int
 */
function modulosProfesorObjeto(){
    if(count(\Auth::user()->profesor()->get()) != 0){
        return \Auth::user()->profesor()->get()->first()->modulos();
    }
    return 0;
}

/**
 * @return mixed
 */
function profesor(){
    return \Auth::user()->get()->first()->nombre;
}

/**
 * @return int
 */
function cuantasUfs(){
    $total = 0;
    if(modulosProfesor() != 0){
        foreach (modulosProfesorObjeto()->get() as $modulo){
            $total += count($modulo->ufs()->get());
        }
    }
    return $total;
}

/**
 * @return int
 */
function cuantosNfs(){
    $total = 0;
    if(modulosProfesor() != 0){
        foreach (modulosProfesorObjeto()->get() as $modulo){
            foreach ($modulo->ufs()->get() as $uf){
                $total += count($uf->nfs()->get());
            }
        }
    }
    return $total;
}

/**
 * @return int
 */
function cuantosItems(){
    $total = 0;
    if(modulosProfesor() != 0){
        foreach (modulosProfesorObjeto()->get() as $modulo){
            foreach ($modulo->ufs()->get() as $uf){
                foreach ($uf->nfs()->get() as $nf){
                    $total += count($nf->items()->get());
                }
            }
        }
    }
    return $total;
}

/**
 * @return mixed
 */
function randomimg(){
    $imagenes = [
        ['item1.png'],
        ['item2.png'],
        ['item3.png'],
        ['item4.png'],
        ['item5.png'],
        ['item6.png']
    ];
    return array_random($imagenes);
}

/**
 * @return int
 */
function cuantosAlumnos(){
    return count(\App\Alumno::all());
}