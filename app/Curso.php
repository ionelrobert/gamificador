<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    protected $table = "cursos";
    protected $primaryKey = 'id';
    protected $fillable = [
        'nombre', 'grupo'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function alumnos()
    {
        return $this->belongsToMany(Alumno::class, 'alumno_curso' ,'curso_id', 'alumno_id');
    }
}
