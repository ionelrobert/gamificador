<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = "items";
    protected $primaryKey = 'id';
    protected $fillable = [
        'nombre', 'valorMaximo','examen','activo','nf_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function alumnos()
    {
        return $this->belongsToMany(Alumno::class,'alumno_item','item_id','alumno_id')->withPivot('valoracion','nota');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function nf()
    {
        return $this->belongsTo(NF::class);
    }
    public function contains($id_item, $id_alumno){
        $encontrado = false;
        foreach ($this->alumnos()->get() as $alumno){
            foreach ($alumno->items()->get() as $item){
                if($alumno->user_id == $id_alumno){
                    if($item->id == $id_item){
                        $encontrado = true;
                    }
                }
            }
        }
        return $encontrado;
    }

    /**
     * @param Item $item
     * @return bool
     */
    public function isExamen(Item $item){
        return $item->examen == 1;
    }
}
