<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NF extends Model
{
    protected $table = "nfs";
    protected $primaryKey = 'id';
    protected $fillable = [
        'nombre','peso','uf_id','modulo_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function uf()
    {
        return $this->belongsTo(UF::class, 'uf_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany(Item::class, 'nf_id');
    }
}
