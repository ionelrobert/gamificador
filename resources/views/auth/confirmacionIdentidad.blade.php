@extends('auth.layout.layout')
@section('content')
		<div class="brand">
			<img 	@if(\Auth::user()->imagen != null)
						@if(\Auth::user()->roles()->first()->name != 'alumno')
							src="{{url('perfiles/profesores/'.\Auth::user()->imagen)}}"
						@else
							src="{{url('perfiles/alumnos/'.\Auth::user()->imagen)}}"
						@endif
					@else
						src="{{url('profesores/assets/img/logo.png')}}"
					@endif
					class="img-fluid" style="background-size: contain">
		</div>
		<div class="card fat btn-">
			<div class="card-body ">
				<h4 class="card-title">Confirma tu idéntidad</h4>
				<form method="POST" action="{{ route('confirmarIdentidad') }}">
					{{csrf_field()}}
					<div class="form-group">
						<label for="email">{{\Auth::user()->email}}</label>

						<input id="email" type="email" class="form-control" name="email" value="{{\Auth::user()->email}}" required autofocus readonly hidden>
						@if ($errors->has('email'))
							<span class="help-block">
								<strong>{{ $errors->first('email') }}</strong>
                            </span>
						@endif
					</div>

					<div class="form-group">
						<label for="password">Password
							<!--<a href="{{url('password/reset')}}" class="float-right">
								¿Has olvidado la contraseña?
							</a>-->
						</label>
						<input id="password" type="password" class="form-control" name="password" required data-eye>
						@if ($errors->has('password'))
							<span class="help-block">
                                 <strong>{{ $errors->first('password') }}</strong>
                            </span>
						@endif
					</div>

					<div class="form-group no-margin">
						<button type="submit" class="btn btn-primary btn-block">
							Confirmar
						</button>
					</div>
				</form>
			</div>
		</div>
@endsection
