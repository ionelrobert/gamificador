@extends('auth.layout.layout')
@section('content')
		<div class="brand">
			<img src="{{url('profesores/assets/img/logo.png')}}" class="img-fluid" style="background-size: contain">
		</div>
		<div class="card fat btn-">
			<div class="card-body ">
				<h4 class="card-title">Login</h4>
				<form method="POST" action="{{ url('login') }}">
					{{csrf_field()}}
					<div class="form-group">
						<label for="email">E-Mail</label>

						<input id="email" type="email" class="form-control" name="email" value="" required autofocus>
						@if ($errors->has('email'))
							<span class="help-block">
								<strong>{{ $errors->first('email') }}</strong>
                            </span>
						@endif
					</div>

					<div class="form-group">
						<label for="password">Password
							<a href="{{url('password/reset')}}" class="float-right">
								¿Has olvidado la contraseña?
							</a>
						</label>
						<input id="password" type="password" class="form-control" name="password" required data-eye>
						@if ($errors->has('password'))
							<span class="help-block">
								<strong>{{ $errors->first('password') }}</strong>
                            </span>
						@endif
					</div>

					<div class="form-group no-margin">
						<button type="submit" class="btn btn-primary btn-block">
							Login
						</button>
					</div>
					<div class="margin-top20 text-center">
						¿No tienes cuenta? <a href="{{route('register')}}">Create una</a>
					</div>
				</form>
			</div>
		</div>
@endsection
