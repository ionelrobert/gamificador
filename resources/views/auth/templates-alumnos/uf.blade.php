@extends('auth.alumnos.layout')
@section('content')
    <style>
        canvas{
            margin-left: 0px !important;
        }
    </style>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h1><strong>{{$uf->nombre}}</strong></h1>
            </div>
        </div>
        <div class="row">
            <!--<div class="col-md-12">-->
                <!--<h2><strong>NF 1:</strong> Lorem Ipsum</h2>-->
                <!--<div class="card-deck">-->
                    @if(count($items)>0)
                        @foreach($items as $item)
                            <div class="col-md-3">
                                <br>
                                <!--<p class="text-uppercase"><strong>{{$item->nf()->first()->nombre}}</strong></p>-->
                                <div class="card card-cascade wider">
                                    <div class="view gradient-card-header @if($item->examen == 1) blue-gradient @else purple-gradient @endif">
                                        <!-- Title -->
                                        <h6 class="card-header-title mb-3"><b>{{$item->nf()->first()->nombre}}</b></h6>
                                        <!-- Subtitle -->
                                        <p class="card-header-subtitle mb-0">
                                            @if($item->examen == 1)
                                                <i class="fa fa-bomb black-text"></i>&nbsp;&nbsp;Examen ({{$item->valorMaximo}}%)
                                            @else
                                                @if($item->valorMaximo != 0)
                                                    <b><i class="fas fa-tasks mr2 black-text"></i>&nbsp;&nbsp;<span class="black-text">Práctica ({{$item->valorMaximo}}%)</span></b>
                                                @else
                                                    <i class="far fa-file-alt mr2 black-text"></i>&nbsp;&nbsp;Teoria
                                                @endif
                                            @endif
                                        </p>

                                    </div>

                                    <div class="card-body @if($item->examen == 1) secondary-color-dark white-text @endif">
                                        <h4 class="card-title"><strong>{{$item->nombre}}</strong></h4>

                                        <div class="progress" style="height: 20px">
                                            <div class="progress-bar progress-bar-striped progress-bar-animated"
                                                 role="progressbar"
                                                 style="@if($item->pivot->valoracion<20)
                                                         width: 20%;
                                                        @else
                                                         width: {{$item->pivot->valoracion}}%;
                                                        @endif
                                                        height: 20px">
                                                {{$item->pivot->valoracion}}%
                                            </div>
                                        </div>
                                        <span>Nota: {{$item->pivot->nota}}</span>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <p class="text-center">El profesor todavía, no ha corregido las prácticas</p>
                    @endif
        </div>
        @if(count($items)>0)
            <br><br>
            <div class="jumbotron">
                <h1 class="text-center pink-text"><b>Información sobre ti</b></h1>
                <!--<button class="btn btn-primary">Duro contra el examen</button>-->
                <hr class="my-2 mb-5">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified pink" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#estado" role="tab">
                            <!--<i class="fa fa-user"></i> Estado-->
                            <img src="{{url('iconos/estado.png')}}" alt="" height="70" width="70">
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#ranking" role="tab">
                            <!--<i class="fa fa-heart"></i> Ranking-->
                            <img src="{{url('iconos/ranking2.png')}}" alt="" height="70" width="70">
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#guerra" role="tab">
                            <!--<i class="fa fa-heart"></i> Ranking-->
                            <img src="{{url('iconos/guerra.png')}}" alt="" height="70" width="70">
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#juego" role="tab">
                            <!--<i class="fa fa-heart"></i> Ranking-->
                            <img src="{{url('iconos/alumnos.png')}}" alt="" height="70" width="70">
                        </a>
                    </li>
                </ul>
                <!-- Tab panels -->
                <div class="tab-content">
                    <!--Panel 1-->
                    <div class="tab-pane fade in show active" id="estado" role="tabpanel">
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                @if(\Auth::user()->alumno()->first()->logros($uf)==1)
                                    <img src="{{url('alumnos/avatares/'.\Auth::user()->alumno()->first()->personaje.'_idle.gif')}}" alt="">
                                @elseif(\Auth::user()->alumno()->first()->logros($uf)==2)
                                    <img src="{{url('alumnos/avatares/'.\Auth::user()->alumno()->first()->personaje.'_walk.gif')}}" alt="">
                                @elseif(\Auth::user()->alumno()->first()->logros($uf)==3)
                                    <img src="{{url('alumnos/avatares/'.\Auth::user()->alumno()->first()->personaje.'_run.gif')}}" alt="">
                                @else
                                    <img src="{{url('alumnos/avatares/'.\Auth::user()->alumno()->first()->personaje.'_attack.gif')}}" alt="">
                                @endif
                                <hr class="my-2">
                            </div>
                            <div class="col-md-6">
                                <h1 class="text-center"><span class="badge pink">{{\Auth::user()->alumno()->first()->nombre}} {{\Auth::user()->alumno()->first()->apellidos}}</span></h1>
                                <br>
                                <div class="row">
                                    <div class="col-md-2">
                                        <h5><span class="badge badge-success">Salud</span></h5>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="progress mt-1" style="height: 20px">
                                            <div class="progress-bar bg-success progress-bar-striped progress-bar-animated"
                                                 role="progressbar"
                                                 style="@if(\Auth::user()->alumno()->first()->salud<20)
                                                         width: 20%;
                                                 @else
                                                         width: {{\Auth::user()->alumno()->first()->salud}}%;
                                                 @endif
                                                         height: 20px">
                                                {{\Auth::user()->alumno()->first()->salud}}%
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="my-2">
                                <div class="row">
                                    <div class="col-md-2">
                                        <h5><span class="badge badge-primary">Energia</span></h5>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="progress mt-1" style="height: 20px">
                                            <div class="progress-bar progress-bar-striped progress-bar-animated text-center"
                                                 role="progressbar"
                                                 style="width: {{\Auth::user()->alumno()->first()->valoracionItems($uf)}}%;height: 20px">
                                                {{\Auth::user()->alumno()->first()->notaUF($uf)}}%
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="my-2">
                                <div class="row">
                                    <div class="col-md-6">
                                        @if(\Auth::user()->alumno()->first()->logros($uf)==1)
                                            @for($i = 0;$i<3;$i++)
                                                <img src="{{url('alumnos/assets/logros/logro_locked.png')}}" alt="" height="50" width="50">
                                            @endfor
                                        @elseif(\Auth::user()->alumno()->first()->logros($uf)==2)

                                                <img src="{{url('alumnos/assets/logros/logo_ordinador.png')}}" alt="" height="50" width="50">
                                            @for($i = 0;$i<2;$i++)
                                                    <img src="{{url('alumnos/assets/logros/logro_locked.png')}}" alt="" height="50" width="50">
                                                @endfor
                                        @elseif(\Auth::user()->alumno()->first()->logros($uf)==3)
                                                <img src="{{url('alumnos/assets/logros/logo_ordinador.png')}}" alt="" height="50" width="50">
                                                <img src="{{url('alumnos/assets/logros/logro_apunts.png')}}" alt="" height="50" width="50">
                                                <img src="{{url('alumnos/assets/logros/logro_locked.png')}}" alt="" height="50" width="50">
                                        @else
                                                <img src="{{url('alumnos/assets/logros/logo_ordinador.png')}}" alt="" height="50" width="50">
                                                <img src="{{url('alumnos/assets/logros/logro_apunts.png')}}" alt="" height="50" width="50">
                                                <img src="{{url('alumnos/assets/logros/logo_google.png')}}" alt="" height="50" width="50">
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/.Panel 1-->
                    <!--Panel 2-->
                    <div class="tab-pane fade" id="ranking" role="tabpanel">
                        <br>
                        <ul class="list-group">
                            @foreach($uf->ranking() as $key => $rank)
                                @if($key == \Auth::user()->id)
                                    <li class="list-group-item d-flex justify-content-between align-items-center active">
                                        {{$rank['nombre']}}
                                        <span class="badge badge-pill stylish-color-dark">{{$rank['nota']}}</span>
                                    </li>
                                @else
                                    <li class="list-group-item d-flex justify-content-between">
                                        {{$rank['nombre']}}
                                        <span class="badge badge-primary badge-pill">{{$rank['nota']}}</span>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                    <!--/.Panel 2-->
                    <!--Panel 3-->
                    <div class="tab-pane fade" id="guerra" role="tabpanel">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-6">
                                    <h1 class="text-center"><span class="badge pink">Tú</span></h1>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <h5><span class="badge badge-success">Salud</span></h5>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="progress mt-1" style="height: 20px">
                                                <div class="progress-bar bg-success progress-bar-striped progress-bar-animated"
                                                     role="progressbar"
                                                     style="@if(\Auth::user()->alumno()->first()->salud<20)
                                                             width: 20%;
                                                            @else
                                                             width: {{\Auth::user()->alumno()->first()->salud}}%;
                                                            @endif
                                                             height: 20px">
                                                    {{\Auth::user()->alumno()->first()->salud}}%
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="my-2">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <h5><span class="badge badge-primary">Energia</span></h5>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="progress mt-1" style="height: 20px">
                                                <div class="progress-bar progress-bar-striped progress-bar-animated text-center"
                                                     role="progressbar"
                                                     style="width: {{\Auth::user()->alumno()->first()->valoracionItems($uf)}}%;height: 20px">
                                                    {{\Auth::user()->alumno()->first()->notaUF($uf)}}%
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="my-2">
                                    <div class="row">
                                        <div class="col-md-12">
                                            @if(\Auth::user()->alumno()->first()->logros($uf)==1)
                                                @for($i = 0;$i<3;$i++)
                                                    <img src="{{url('alumnos/assets/logros/logro_locked.png')}}" alt="" height="50" width="50">
                                                @endfor
                                            @elseif(\Auth::user()->alumno()->first()->logros($uf)==2)

                                                <img src="{{url('alumnos/assets/logros/logo_ordinador.png')}}" alt="" height="50" width="50">
                                                @for($i = 0;$i<2;$i++)
                                                    <img src="{{url('alumnos/assets/logros/logro_locked.png')}}" alt="" height="50" width="50">
                                                @endfor
                                            @elseif(\Auth::user()->alumno()->first()->logros($uf)==3)
                                                <img src="{{url('alumnos/assets/logros/logo_ordinador.png')}}" alt="" height="50" width="50">
                                                <img src="{{url('alumnos/assets/logros/logro_apunts.png')}}" alt="" height="50" width="50">
                                                <img src="{{url('alumnos/assets/logros/logro_locked.png')}}" alt="" height="50" width="50">
                                            @else
                                                <img src="{{url('alumnos/assets/logros/logo_ordinador.png')}}" alt="" height="50" width="50">
                                                <img src="{{url('alumnos/assets/logros/logro_apunts.png')}}" alt="" height="50" width="50">
                                                <img src="{{url('alumnos/assets/logros/logo_google.png')}}" alt="" height="50" width="50">
                                            @endif
                                        </div>
                                    </div>
                                    <hr class="my-2">
                                    <div class="row">
                                        <div class="col-md-10">
                                            @if(\Auth::user()->alumno()->first()->logros($uf)==1)
                                                <img src="{{url('alumnos/avatares/'.\Auth::user()->alumno()->first()->personaje.'_idle.gif')}}" alt="">
                                            @elseif(\Auth::user()->alumno()->first()->logros($uf)==2)
                                                <img src="{{url('alumnos/avatares/'.\Auth::user()->alumno()->first()->personaje.'_walk.gif')}}" alt="">
                                            @elseif(\Auth::user()->alumno()->first()->logros($uf)==3)
                                                <img src="{{url('alumnos/avatares/'.\Auth::user()->alumno()->first()->personaje.'_run.gif')}}" alt="">
                                            @else
                                                <img src="{{url('alumnos/avatares/'.\Auth::user()->alumno()->first()->personaje.'_attack.gif')}}" alt="">
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    @php
                                        $alumno = $uf->randomAlumno(\Auth::user()->alumno()->first()->user_id);
                                    @endphp
                                    <h1 class="text-center"><span class="badge pink">
                                            {{$alumno->nombre}}
                                            {{$alumno->apellidos}}
                                        </span></h1>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <h5><span class="badge badge-success">Salud</span></h5>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="progress mt-1" style="height: 20px">
                                                <div class="progress-bar bg-success progress-bar-striped progress-bar-animated"
                                                     role="progressbar"
                                                     style="@if($alumno->salud<20)
                                                             width: 20%;
                                                     @else
                                                             width: {{$alumno->salud}}%;
                                                     @endif
                                                             height: 20px">
                                                    {{$alumno->salud}}%
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="my-2">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <h5><span class="badge badge-primary">Energia</span></h5>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="progress mt-1" style="height: 20px">
                                                <div class="progress-bar progress-bar-striped progress-bar-animated text-center"
                                                     role="progressbar"
                                                     style="width: {{$alumno->valoracionItems($uf)}}%;height: 20px">
                                                    {{$alumno->notaUF($uf)}}%
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="my-2">
                                    <div class="row">
                                        <div class="col-md-12">
                                            @if($alumno->logros($uf)==1)
                                                @for($i = 0;$i<3;$i++)
                                                    <img src="{{url('alumnos/assets/logros/logro_locked.png')}}" alt="" height="50" width="50">
                                                @endfor
                                            @elseif($alumno->logros($uf)==2)

                                                <img src="{{url('alumnos/assets/logros/logo_ordinador.png')}}" alt="" height="50" width="50">
                                                @for($i = 0;$i<2;$i++)
                                                    <img src="{{url('alumnos/assets/logros/logro_locked.png')}}" alt="" height="50" width="50">
                                                @endfor
                                            @elseif($alumno->logros($uf)==3)
                                                <img src="{{url('alumnos/assets/logros/logo_ordinador.png')}}" alt="" height="50" width="50">
                                                <img src="{{url('alumnos/assets/logros/logro_apunts.png')}}" alt="" height="50" width="50">
                                                <img src="{{url('alumnos/assets/logros/logro_locked.png')}}" alt="" height="50" width="50">
                                            @else
                                                <img src="{{url('alumnos/assets/logros/logo_ordinador.png')}}" alt="" height="50" width="50">
                                                <img src="{{url('alumnos/assets/logros/logro_apunts.png')}}" alt="" height="50" width="50">
                                                <img src="{{url('alumnos/assets/logros/logo_google.png')}}" alt="" height="50" width="50">
                                            @endif
                                        </div>
                                    </div>
                                    <hr class="my-2">
                                    <div class="row">
                                        <div class="col-md-10">
                                            @if($alumno->logros($uf)==1)
                                                <img src="{{url('alumnos/avatares/'.$alumno->personaje.'_idle.gif')}}" alt="">
                                            @elseif($alumno->logros($uf)==2)
                                                <img src="{{url('alumnos/avatares/'.$alumno->personaje.'_walk.gif')}}" alt="">
                                            @elseif($alumno->logros($uf)==3)
                                                <img src="{{url('alumnos/avatares/'.$alumno->personaje.'_run.gif')}}" alt="">
                                            @else
                                                <img src="{{url('alumnos/avatares/'.$alumno->personaje.'_attack.gif')}}" alt="">
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/.Panel 3-->
                    <!--Panel 4-->
                    <div class="tab-pane fade" id="juego" role="tabpanel">

                    </div>
                    <!--/.Panel 4-->
                </div>
            </div>
        @endif
    </div>
@endsection
@section('scripts')
    {!! Html::script('game/phaser.min.js') !!}
    <script>
        window.onload = function() {
            const VELOCIDAD = {{\Auth::user()->alumno()->first()->salud}};
            var game = new Phaser.Game(640,480,Phaser.CANVAS,"juego",{preload:onPreload, create:onCreate, update:onUpdate});

            var player;
            var wallsBitmap;
            var floor;
            var lightAngle = Math.PI/3;
            //var lightAngle = ;
            var numberOfRays = 5;
            var rayLength = {{\Auth::user()->alumno()->first()->notaUF($uf)}};

            function onPreload() {
                game.load.image("floor","{{url('game/img/floor.png')}}");
                game.load.image("walls","{{url('game/img/walls.png')}}");
                game.load.image("player","{{url('game/img/player.png')}}");
                /*
                @if($alumno->logros($uf)==1)
                    game.load.image("player","{{url('alumnos/avatares/'.\Auth::user()->alumno()->first()->personaje.'_idle.gif')}}");
                @elseif($alumno->logros($uf)==2)
                    game.load.image("player","{{url('alumnos/avatares/'.\Auth::user()->alumno()->first()->personaje.'_walk.gif')}}");
                @elseif($alumno->logros($uf)==3)
                    game.load.image("player","{{url('alumnos/avatares/'.\Auth::user()->alumno()->first()->personaje.'_run.gif')}}");
                @else
                    game.load.image("player","{{url('alumnos/avatares/'.\Auth::user()->alumno()->first()->personaje.'_attack.gif')}}");
                @endif
                */
            }

            function goFullScreen(){
                game.scale.pageAlignHorizontally = true;
                game.scale.pageAlignVertically = true;
                game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
                game.scale.setScreenSize(true);
            }

            function onCreate() {
                goFullScreen();
                floor = game.add.sprite(0,0,"floor");
                wallsBitmap = game.make.bitmapData(640,480);
                wallsBitmap.draw("walls");
                wallsBitmap.update();
                game.add.sprite(0,0,wallsBitmap);
                player = game.add.sprite(80,80,"player");
                player.anchor.setTo(0.5,0.5);
                player.enableBody = true;
                cursors = game.input.keyboard.createCursorKeys();
                maskGraphics = this.game.add.graphics(0, 0);
                floor.mask=maskGraphics
            }

            function onUpdate() {
                var xSpeed = 0;
                var ySpeed = 0;
                if(cursors.up.isDown){
                    //ySpeed -= {{\Auth::user()->alumno()->first()->salud}};
                    ySpeed -=1;
                }
                if(cursors.down.isDown){
                    //ySpeed += {{\Auth::user()->alumno()->first()->salud}};
                    ySpeed +=1;
                }
                if(cursors.left.isDown){
                    //xSpeed -= {{\Auth::user()->alumno()->first()->salud}};
                    xSpeed -=1;
                }
                if(cursors.right.isDown){
                    //xSpeed += {{\Auth::user()->alumno()->first()->salud}};;
                    xSpeed +=1;
                }
                if(Math.abs(xSpeed)+Math.abs(ySpeed)<2 && Math.abs(xSpeed)+Math.abs(ySpeed)>0){
                    var color = wallsBitmap.getPixel32(player.x+xSpeed+player.width/2,player.y+ySpeed+player.height/2);
                    color+= wallsBitmap.getPixel32(player.x+xSpeed-player.width/2,player.y+ySpeed+player.height/2);
                    color+=wallsBitmap.getPixel32(player.x+xSpeed-player.width/2,player.y+ySpeed-player.height/2)
                    color+=wallsBitmap.getPixel32(player.x+xSpeed+player.width/2,player.y+ySpeed-player.height/2)
                    if(color==0){
                        player.x+=xSpeed;
                        player.y+=ySpeed;
                        //player.x+={{\Auth::user()->alumno()->first()->salud / 5}};
                        //player.y+={{\Auth::user()->alumno()->first()->salud / 5}};
                    }
                }
                var mouseAngle = Math.atan2(player.y-game.input.y,player.x-game.input.x);
                maskGraphics.clear();
                maskGraphics.lineStyle(2, 0xffffff, 1);
                maskGraphics.beginFill(0xffff00);
                maskGraphics.moveTo(player.x,player.y);
                for(var i = 0; i<numberOfRays; i++){
                    var rayAngle = mouseAngle-(lightAngle/2)+(lightAngle/numberOfRays)*i
                    var lastX = player.x;
                    var lastY = player.y;
                    for(var j= 1; j<=rayLength;j+=1){
                        var landingX = Math.round(player.x-(2*j)*Math.cos(rayAngle));
                        var landingY = Math.round(player.y-(2*j)*Math.sin(rayAngle));
                        if(wallsBitmap.getPixel32(landingX,landingY)==0){
                            lastX = landingX;
                            lastY = landingY;
                        }
                        else{
                            maskGraphics.lineTo(lastX,lastY);
                            break;
                        }
                    }
                    maskGraphics.lineTo(lastX,lastY);
                }
                maskGraphics.lineTo(player.x,player.y);
                maskGraphics.endFill();
                floor.alpha = 0.5+Math.random()*0.5;
            }
        }
    </script>
@endsection
