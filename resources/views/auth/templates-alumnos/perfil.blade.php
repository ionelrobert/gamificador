@extends('auth.alumnos.layout')
@section('content')
    <style>
        .btn-file {
            position: relative;
            overflow: hidden;
        }
        .btn-file input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            min-width: 100%;
            min-height: 100%;
            font-size: 100px;
            text-align: right;
            filter: alpha(opacity=0);
            opacity: 0;
            outline: none;
            background: white;
            cursor: inherit;
            display: block;
        }
    </style>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @include('auth.partials.success-warning')
                @include('auth.partials.errores')
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 text-center">
                <h1><strong>Mi Perfil</strong></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- Card -->
                <div class="card testimonial-card">

                    <!-- Bacground color -->
                    <div class="card-up indigo lighten-1">
                    </div>

                    <!-- Avatar -->
                    <div class="avatar mx-auto white"><img @if(\Auth::user()->imagen) src="{{url('perfiles/alumnos/'.\Auth::user()->imagen)}}" @else src="{{url('perfiles/foto-perfil.jpg')}}" @endif class="rounded-circle">
                    </div>

                    <div class="card-body">
                        <!-- Name -->
                        <h4 class="card-title">{{\Auth::user()->alumno()->first()->nombre}} {{\Auth::user()->alumno()->first()->apellidos}}</h4>
                        <hr>
                        <!-- Quotation -->
                        <p>
                        {{ Form::open(['url' => '/alumnos/miperfil/editar', 'method' => 'post', 'role' => 'form', 'files' => true]) }}
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label class="control-label">Dirección Email</label>
                                    <input type="email" id="email" name="email" class="form-control" value="{{$usuario->email}}" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Nombre</label>
                                    <input type="text" id="nombre" name="nombre" class="form-control" value="{{$usuario->name}}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Apellidos</label>
                                    @if($usuario->alumno)
                                        <input type="text" id="apellidos" name="apellidos" class="form-control" value="{{$usuario->alumno->apellidos}}">
                                    @else
                                        <input type="text" id="apellidos" name="apellidos" class="form-control">
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Contraseña</label>
                                    <input type="password" id="password" name="password" class="form-control" data-eye>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Confirmar contraseña</label>
                                    <input type="password" id="password-confirm" name="password_confirmation" class="form-control" data-eye>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <span class="btn btn-info btn-file">
                                        Cambiar foto de perfil <input type="file" id="imagen" name="imagen">
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 d-flex justify-content-center">
                                <select class="mdb-select" id="personaje" name="personaje">
                                    <option value="" disabled selected>Escoje tu personaje</option>
                                    <option value="hero1" data-icon="{{url('alumnos/avatares/hero1.png')}}" class="rounded-circle" @if(\Auth::user()->alumno()->first()->heroe('hero1') == 1) selected @endif>Charly</option>
                                    <option value="hero2" data-icon="{{url('alumnos/avatares/hero2.png')}}" class="rounded-circle" @if(\Auth::user()->alumno()->first()->heroe('hero2') == 1) selected @endif>Inicapitan</option>
                                    <option value="hero3" data-icon="{{url('alumnos/avatares/hero3.png')}}" class="rounded-circle" @if(\Auth::user()->alumno()->first()->heroe('hero3') == 1) selected @endif>Kigrin</option>
                                    <option value="hero4" data-icon="{{url('alumnos/avatares/hero4.png')}}" class="rounded-circle" @if(\Auth::user()->alumno()->first()->heroe('hero4') == 1) selected @endif>Gonzineitor</option>
                                    <option value="hero5" data-icon="{{url('alumnos/avatares/hero5.png')}}" class="rounded-circle" @if(\Auth::user()->alumno()->first()->heroe('hero5') == 1) selected @endif>Fergolos</option>
                                    <option value="hero6" data-icon="{{url('alumnos/avatares/hero6.png')}}" class="rounded-circle" @if(\Auth::user()->alumno()->first()->heroe('hero6') == 1) selected @endif>Tronx</option>
                                </select>
                                <label>Personaje</label>

                            </div>
                        </div>
                        <input type="hidden" id="id" name="id" class="form-control" value="{{$usuario->id}}" readonly="true" required>
                        {!! Form::submit('Actualizar Perfil', ['class' => 'btn btn-primary pull-right']) !!}
                        <div class="row">
                            <div class="col-md-4">
                                <img src="{{url('alumnos/avatares/hero1.png')}}" alt="" height="80%">
                                <p id="passwordHelpInlineMD" class="text-muted">
                                    <strong>Chraly</strong>
                                </p>
                            </div>
                            <div class="col-md-4">
                                <img src="{{url('alumnos/avatares/hero2.png')}}" alt="" height="80%">
                                <p id="passwordHelpInlineMD" class="text-muted">
                                    <strong>Inicapitan</strong>
                                </p>
                            </div>
                            <div class="col-md-4">
                                <img src="{{url('alumnos/avatares/hero3.png')}}" alt="" height="80%">
                                <p id="passwordHelpInlineMD" class="text-muted">
                                    <strong>Kigrin</strong>
                                </p>
                            </div>
                            <div class="col-md-4">
                                <img src="{{url('alumnos/avatares/hero4.png')}}" alt="" height="80%">
                                <p id="passwordHelpInlineMD" class="text-muted">
                                    <strong>Gonzineitor</strong>
                                </p>
                            </div>
                            <div class="col-md-4">
                                <img src="{{url('alumnos/avatares/hero5.png')}}" alt="" height="80%">
                                <p id="passwordHelpInlineMD" class="text-muted">
                                    <strong>Fergolos</strong>
                                </p>
                            </div>
                            <div class="col-md-4">
                                <img src="{{url('alumnos/avatares/hero6.png')}}" alt="" height="80%">
                                <p id="passwordHelpInlineMD" class="text-muted">
                                    <strong>Tronx</strong>
                                </p>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        {{ Form::close() }}
                    </div>

                </div>
                <!-- Card -->
            </div>
        </div>

    </div>
@endsection
@section('scripts')
    <!-- {!! Html::script('alumnos/assets/js/dashboard.js') !!} -->
@endsection
