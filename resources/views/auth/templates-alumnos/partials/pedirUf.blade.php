@extends('auth.alumnos.layout')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h1><strong></strong> UF - <h2>{{\Auth::user()->alumno()->first()->nombre}} {{\Auth::user()->alumno()->first()->apellidos}}</h2></h1>
            </div>
        </div>
        <div class="row">

        <h2><strong>NF 1:</strong> Lorem Ipsum</h2>
              <div class="card-deck">

                  <div class="card card-cascade wider  ">

                      <div class="view zoom">
                          <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Slides/img%20(70).jpg" alt="Card image cap">
                          <a href="#!">
                              <div class="mask rgba-white-slight"></div>
                          </a>
                      </div>

                      <div class="card-body z-depth-1-half">

                          <h6 class="font-weight-bold indigo-text py-2"><i class="far fa-file-alt mr2"></i>&nbsp;&nbsp;Teoria</h6>
                          <h4 class="card-title"><strong>Teoria JavaScript</strong></h4>


                      </div>

                  </div>

                  <div class="card card-cascade wider">

                      <div class="view overlay">
                          <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Slides/img%20(70).jpg" alt="Card image cap">
                          <a href="#!">
                              <div class="mask rgba-white-slight"></div>
                          </a>
                      </div>

                      <div class="card-body elegant-color">

                          <h6 class="font-weight-bold lime-text py-2"><i class="far fa-file-alt mr2"></i>&nbsp;&nbsp;Teoria</h6>
                          <h4 class="card-title white-text"><strong>Teoria JavaScript</strong></h4>
                      </div>

                  </div>

                  <div class="card card-cascade wider">
                      <div class="view overlay">
                          <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Slides/img%20(70).jpg" alt="Card image cap">
                          <a href="#!">
                              <div class="mask rgba-white-slight"></div>
                          </a>
                      </div>

                      <div class="card-body ">

                          <h6 class="font-weight-bold cyan-text py-2"><i class="fas fa-tasks mr2"></i>&nbsp;&nbsp;Tarea</h6>
                          <h4 class="card-title"><strong>Practica JavaScript</strong></h4>

                          <div class="progress" style="height: 10px">
                              <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: 45%; height: 10px" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100">55%</div>
                          </div>
                      </div>

                  </div>

                  <div class="card card-cascade wider">

                      <div class="view overlay">
                          <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Slides/img%20(70).jpg" alt="Card image cap">
                          <a href="#!">
                              <div class="mask rgba-white-slight"></div>
                          </a>
                      </div>

                      <div class="card-body ">

                          <h6 class="font-weight-bold cyan-text py-2"><i class="fas fa-tasks mr2"></i>&nbsp;&nbsp;Tarea</h6>
                          <h4 class="card-title"><strong>Practica JavaScript</strong></h4>

                          <div class="progress" style="height: 10px">
                              <div class="progress-bar cyan progress-bar-striped progress-bar-animated" role="progressbar" style="width: 45%; height: 10px" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100">55%</div>
                          </div>
                      </div>

                  </div>

                  <div class="card card-cascade wider">

                      <div class="view overlay">
                          <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Slides/img%20(70).jpg" alt="Card image cap">
                          <a href="#!">
                              <div class="mask rgba-white-slight"></div>
                          </a>
                      </div>

                      <div class="card-body ">

                          <h6 class="font-weight-bold green-text py-2"><i class="fab fa-pied-piper-alt mr2"></i>&nbsp;&nbsp;Examen</h6>
                          <h4 class="card-title"><strong>Examen JavaScript</strong></h4>

                          <div class="progress" style="height: 10px">
                              <div class="progress-bar bg-success progress-bar-striped progress-bar-animated" role="progressbar" style="width: 80%; height: 10px" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100">80%</div>
                          </div>
                      </div>

                  </div>
              </div>

        </div>
    </div>
@endsection
@section('scripts')
    <!-- {!! Html::script('alumnos/assets/js/dashboard.js') !!} -->
@endsection
