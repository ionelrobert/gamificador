<div class="container-fluid">
        <ul class="pull-left">
            <li>
                <a href="http://agora.xtec.cat/iesjoaquimmir/moodle/"
                   target="_blank">
                    Moodle
                </a>
            </li>
            <li>
                <a href="http://wiki.iesjoaquimmir.cat/wiki/index.php/P%C3%A0gina_principal"
                   target="_blank">
                    WikiJmir
                </a>
            </li>
            <li>
                <a href="http://agora.xtec.cat/iesjoaquimmir/"
                   target="_blank">
                    INS Joquimmir
                </a>
            </li>
            <!--<li>
                <a href="#">
                    Blog
                </a>
            </li>-->
        </ul>
    <p class="copyright pull-right">
        &copy;
        <script>
            document.write(new Date().getFullYear())
        </script>
        Sitio web realizado por Marc Iniesta González e Ionel-Robert Duca para <a href="https://agora.xtec.cat/iesjoaquimmir/">INS Joaquim Mir</a>,
    </p>
</div>