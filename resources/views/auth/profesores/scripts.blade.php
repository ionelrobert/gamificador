<!--   Core JS Files   -->
{!! Html::script('profesores/assets/js/jquery-3.2.1.min.js') !!}
{!! Html::script('profesores/assets/js/bootstrap.min.js') !!}
{!! Html::script('profesores/assets/js/material.min.js') !!}
{!! Html::script('profesores/assets/js/swal.js') !!}
{!! Html::script('profesores/assets/js/custom.js') !!}
{!! Html::script('js/dataTables.min.js') !!}
<script>
    $(document).ready(function() {
        $('#example').DataTable( {
            "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100,"Todos"]],
            "language": {
                "url": "{{url('json/Spanish.json')}}"
            }
        } );
    } );
</script>

<!--  Charts Plugin -->
{!! Html::script('profesores/assets/js/chartist.min.js') !!}

<!--  Dynamic Elements plugin -->
{!! Html::script('profesores/assets/js/arrive.min.js') !!}

<!--  PerfectScrollbar Library -->
{!! Html::script('profesores/assets/js/perfect-scrollbar.jquery.min.js') !!}

<!--  Notifications Plugin    -->
{!! Html::script('profesores/assets/js/bootstrap-notify.js') !!}

<!--  Google Maps Plugin    -->
{!! Html::script('https://maps.googleapis.com/maps/api/js?key=AIzaSyCrXHxNzlMcJ8wFjVrBZcMtrojvmzyvBTQ') !!}

<!-- Material Dashboard javascript methods -->
{!! Html::script('profesores/assets/js/material-dashboard.js?v=1.2.0') !!}

{!! Html::script('profesores/assets/js/bootstrap-select.min.js') !!}


