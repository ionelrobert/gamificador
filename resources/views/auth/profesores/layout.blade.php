<!doctype html>
<html lang="es">
<head>
    @include('auth.profesores.head')
</head>
<body>
<div class="wrapper">
    <div class="sidebar" data-color="purple" data-image="">
        <!--Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"
            Tip 2: you can also add an image using data-image tag
        -->
        @include('auth.profesores.sidebar')

    </div>
    <div class="main-panel">
        <nav class="navbar navbar-transparent navbar-absolute">
            @include('auth.profesores.navbar')
        </nav>
        <div class="content">
            <div class="container-fluid">
                @yield('content')
            </div>
        </div>
        <footer class="footer">
            @include('auth.profesores.footer')
        </footer>
    </div>
</div>

</body>
@include('auth.profesores.scripts')
@yield('scripts')
</html>