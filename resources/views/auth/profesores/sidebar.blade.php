<div class="logo">
    <a href="{{route('dashboardProfesores')}}" class="simple-text">
        <img alt="GAMIFICADOR" src="{{asset('alumnos/assets/img/gamir.png')}}" class="gamificador" />
    </a>
</div>
<div class="sidebar-wrapper">
    <ul class="nav">
        @role('superprofesor')
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-exclamation-circle" aria-hidden="true"></i>
                <p>Administrador</p>
            </a>
            <ul class="dropdown-menu" style="width: 100%;">
                <li class="{{ (\Request::route()->getName() == 'usuarios') ? 'active' : '' }}">
                    <a href="{{route('usuarios')}}">
                        <i><img src="{{url('iconos/usuarios.png')}}" class="img-sidebar"></i>
                        <p>Usuarios</p>
                    </a>
                </li>
                <li class="{{ (\Request::route()->getName() == 'cursos') ? 'active' : '' }}">
                    <a href="{{route('cursos')}}">
                        <i><img src="{{url('iconos/cursos.png')}}" class="img-sidebar"></i>
                        <p>Cursos</p>
                    </a>
                </li>
                <li class="{{ (\Request::route()->getName() == 'profesores') ? 'active' : '' }}">
                    <a href="{{route('profesores')}}">
                        <i><img src="{{url('iconos/profesores.png')}}" class="img-sidebar"></i>
                        <p>Profesores</p>
                    </a>
                </li>
                <li class="{{ (\Request::route()->getName() == 'alumnos') ? 'active' : '' }}">
                    <a href="{{route('alumnos')}}">
                        <i><img src="{{url('iconos/alumnos.png')}}" class="img-sidebar"></i>
                        <p>Alumnos</p>
                    </a>
                </li>
                <li class="{{ (\Request::route()->getName() == 'modulos') ? 'active' : '' }}">
                    <a href="{{route('modulos')}}">
                        <i><img src="{{url('iconos/modulos.png')}}" class="img-sidebar"></i>
                        <p>Módulos</p>
                    </a>
                </li>
                <li class="{{ (\Request::route()->getName() == 'ufs') ? 'active' : '' ||
                              (\Request::route()->getName() == 'ufsDaw') ? 'active' : '' ||
                              (\Request::route()->getName() == 'ufsAsix') ? 'active' : '' ||
                              (\Request::route()->getName() == 'ufsSmx') ? 'active' : ''}}">
                    <a href="{{route('ufs')}}">
                        <i><img src="{{url('iconos/ufs.png')}}" class="img-sidebar"></i>
                        <p>UUFF</p>
                    </a>
                </li>
            </ul>
        </li>
        @endrole
        <li class="{{ (\Request::route()->getName() == 'dashboardProfesores') ? 'active' : '' }}"><!-- permisos -->
         <a href="{{route('dashboardProfesores')}}">
                <i><img src="{{url('iconos/dashboard.png')}}" class="img-sidebar"></i>
                <p>Dashboard</p>
            </a>
        </li>
        <li class="{{ (\Request::route()->getName() == 'ufsProfesor') ? 'active' : '' }}">
            <a href="{{route('ufsProfesor')}}">
                <i><img src="{{url('iconos/misufs.png')}}" class="img-sidebar"></i>
                <p>Mis UUFF</p>
            </a>
        </li>
        <li class="{{ (\Request::route()->getName() == 'nfsProfesor') ? 'active' : '' }}">
            <a href="{{route('nfsProfesor')}}">
                <i><img src="{{url('iconos/misnfs.png')}}" class="img-sidebar"></i>
                <p>Mis NNFF</p>
            </a>
        </li>
        <li class="{{ (\Request::route()->getName() == 'perfilProfesor') ? 'active' : '' }}">
            <a href="{{route('perfilProfesor')}}">
                <i><img src="{{url('iconos/miperfil.png')}}" class="img-sidebar"></i>
                <p>Mi Perfil</p>
            </a>
        </li>
        <li class="{{ (\Request::route()->getName() == 'logout') ? 'active' : '' }}">
            <a href="{{route('logout')}}"
               onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                <i><img src="{{url('iconos/salir.png')}}" class="img-sidebar"></i>
                <p>Cerrar sesión</p>
            </a>
        </li>
        <!--
        @role('superprofesor')
            <li class="{{ (\Request::route()->getName() == 'usuarios') ? 'active' : '' }}">
                <a href="{{route('usuarios')}}">
                    <i class="fa fa-user" aria-hidden="true"></i>
                    <p>Usuarios</p>
                </a>
            </li>
            <li class="{{ (\Request::route()->getName() == 'cursos') ? 'active' : '' }}">
                <a href="{{route('cursos')}}">
                    <i class="fa fa-hourglass" aria-hidden="true"></i>
                    <p>Cursos</p>
                </a>
            </li>
            <li class="{{ (\Request::route()->getName() == 'profesores') ? 'active' : '' }}">
                <a href="{{route('profesores')}}">
                    <i class="fa fa-users" aria-hidden="true"></i>
                    <p>Profesores</p>
                </a>
            </li>
            <li class="{{ (\Request::route()->getName() == 'alumnos') ? 'active' : '' }}">
                <a href="{{route('alumnos')}}">
                    <i class="fa fa-user-secret" aria-hidden="true"></i>
                    <p>Alumnos</p>
                </a>
            </li>
            <li class="{{ (\Request::route()->getName() == 'roles') ? 'active' : '' }}" style="display: none">
                <a href="{{route('roles')}}">
                    <i class="fa fa-battery-three-quarters" aria-hidden="true"></i>
                    <p>Roles</p>
                </a>
            </li>
            <li class="{{ (\Request::route()->getName() == 'modulos') ? 'active' : '' }}">
                <a href="{{route('modulos')}}">
                    <i class="fa fa-modx" aria-hidden="true"></i>
                    <p>Módulos</p>
                </a>
            </li>
            <li class="{{ (\Request::route()->getName() == 'ufs') ? 'active' : '' ||
                          (\Request::route()->getName() == 'ufsDaw') ? 'active' : '' ||
                          (\Request::route()->getName() == 'ufsAsix') ? 'active' : '' ||
                          (\Request::route()->getName() == 'ufsSmx') ? 'active' : ''}}">
                <a href="{{route('ufs')}}">
                    <i class="fa fa-signal" aria-hidden="true"></i>
                    <p>UFS</p>
                </a>
            </li>
        @endrole
        -->
    </ul>
</div>