<meta charset="utf-8" />
<link rel="icon" type="image/png" href="{{asset('profesores/assets/img/logo.png')}}" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>Dashboard - Profesores</title>
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
<meta name="viewport" content="width=device-width" />
{{Html::style('profesores/assets/css/profesores.css')}}
<!-- Bootstrap core CSS     -->
{{Html::style('https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css')}}
{{Html::style('https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css')}}

<!--  Material Dashboard CSS    -->
{{Html::style('profesores/assets/css/material-dashboard.css?v=1.2.0')}}

{{Html::style('css/font-awesome.min.css')}}

<!--  CSS for Demo Purpose, don't include it in your project     -->
{{Html::style('profesores/assets/css/custom.css')}}

<!--     Fonts and icons     -->
{{Html::style('http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css')}}
{{Html::style('hnts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons')}}

<!-- Bootstrap-select core CSS     -->
{{Html::style('profesores/assets/css/bootstrap-select.min.css')}}

