@extends('auth.layout.layout')
@section('content')
	<div class="brand">
		<img src="{{url('profesores/assets/img/logo.png')}}">
	</div>
	<div class="card fat">
		<div class="card-body">
			<h4 class="card-title">Registro</h4>
			<form method="POST" action="{{ route('register') }}">
				{{ csrf_field() }}
				<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
					<label for="name">Nombre</label>
					<input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

					@if ($errors->has('name'))
						<span class="help-block">
							<strong>{{ $errors->first('name') }}</strong>
						</span>
					@endif
				</div>

				<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
					<label for="email">E-Mail</label>
					<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

					@if ($errors->has('email'))
						<span class="help-block">
							<strong>{{ $errors->first('email') }}</strong>
						</span>
					@endif
				</div>

				<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
					<label for="password">Contraseña</label>
					<input id="password" type="password" class="form-control" name="password" required data-eye>
					@if ($errors->has('password'))
						<span class="help-block">
							<strong>{{ $errors->first('password') }}</strong>
						</span>
					@endif
				</div>

				<div class="form-group">
					<label for="password-confirm">Confirmación Contraseña</label>
					<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required data-eye>

				</div>

				<div class="form-group no-margin">
					<button type="submit" class="btn btn-primary btn-block">
						Registrarme
					</button>
				</div>
				<div class="margin-top20 text-center">
					¿Ya tienes cuenta? <a href="{{url('/')}}">Login</a>
				</div>
			</form>
		</div>
	</div>
@endsection
