<!--Footer-->
<footer class=" ">

    <!--Footer Links-->
    <div class="container-fluid">
        <div class="row">
            <!--Copyright-->
            <div class="col-sm-12 text-center">
                <button class="btn btn-rounded purple-gradient text-center">© 2018 Copyright: Ionel-Robert Duca & Marc Iniesta</button>
            </div>
        </div>
    </div>

</footer>
<!--/.Footer-->
