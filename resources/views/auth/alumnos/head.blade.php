<meta charset="utf-8" />
<link rel="icon" type="image/png" href="{{asset('profesores/assets/img/logo.png')}}" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>Dashboard Alumnos</title>
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
<meta name="viewport" content="width=device-width" />

{{Html::style('alumnos/assets/css/bootstrap.min.css')}}
{{Html::style('alumnos/assets/css/mdb.min.css')}}
{{Html::style('alumnos/assets/css/compiled.min.css')}}
{{Html::style('https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons')}}
{{Html::style('https://use.fontawesome.com/releases/v5.0.11/css/all.css')}}
{{Html::style('alumnos/assets/css/alumnos.css')}}
