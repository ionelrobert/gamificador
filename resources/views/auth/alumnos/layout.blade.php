<!doctype html>
<html lang="es">
<head>
    @include('auth.alumnos.head')
</head>
<body>
    <div class="fondo">
    @include('auth.alumnos.header')
        @yield('content')
        @for($i=0; $i<30; $i++)
            <br>
        @endfor
    @include('auth.alumnos.footer')
    </div>
</body>
@include('auth.alumnos.scripts')
@yield('scripts')
</html>