<!--<div class="header">
<div class="container-fluid">
    <div class="row header-nav">
        <div class="col-md-4 links-nav">
            <img class="logo" src="{{url('/alumnos/assets/img/gamir.png')}}" alt="Gamir">
        </div>

        <div class="col-md-4 links-nav">
            <h4>Modulos</h4>
            <button type="button" class="btn btn-light-green">Light-green</button>

        </div>

        <div class="col-md-4 links-nav perfil">
            <button type="button" class="foto-perfil" style="background: url('{{url('/alumnos/assets/img/logo.png')}}')"></button><p class="nombre">{{\Auth::user()->name}}          <i class="fa fa-cogs fa-lg" aria-hidden="true"></i></p>
        </div>
    </div>
</div>
-->

<header>
 <div class="contaier-fluid">
    <nav class="navbar navbar-expand-lg navbar-dark">
        <img class="navbar-brand logo" src="{{url('/alumnos/assets/img/gamir.png')}}" alt="Gamir">

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02"
                aria-expanded="false" aria-label="Toggle navigation">
            <i class="fas fa-align-justify black-text fa-lg"></i>
        </button>

        <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
            <ul class="navbar-nav mr-auto">
                <a class="btn btn-rounded {{ (\Request::route()->getName() == 'dashboardAlumnos') ? 'peach-gradient' : 'purple-gradient' }}"
                   role="button"
                   href="{{route('dashboardAlumnos')}}">
                    Dashboard
                </a>
                <a class="btn btn-rounded purple-gradient" role="button" href="{{route('dashboardAlumnos')}}" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">Cerrar Sesión</a>
            </ul>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
            @if(\Auth::user()->alumno()->first() != null)
            <ul class="navbar-nav ml-auto">
                <div class="chip chip-md elegant-color z-depth-1">
                    <img @if(\Auth::user()->imagen) src="{{url('perfiles/alumnos/'.\Auth::user()->imagen)}}" @else src="{{url('perfiles/foto-perfil.jpg')}}" @endif class="img-fluid" alt="">
                    <a class="white-text" href="{{route('perfilAlumno')}}">{{\Auth::user()->alumno()->first()->nombre}} {{\Auth::user()->alumno()->first()->apellidos}}</a>
                </div>
            </ul>
            @endif
        </div>
    </nav>
 </div>
</header>