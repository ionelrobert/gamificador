@extends('auth.layout.layout')
@section('content')
    <div class="card-wrapper">
        <div class="brand">
            <img src="{{url('profesores/assets/img/logo.png')}}">
        </div>
        <div class="card fat">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <div class="card-body">
                <h4 class="card-title">Restablecer contraseña</h4>
                <form method="POST" action="{{ route('password.email') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="email">E-Mail</label>
                        <input id="email" type="email" class="form-control" name="email" value="" required autofocus>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                        <div class="form-text text-muted">
                            Al hacer clic en "Restablecer contraseña", le enviaremos un enlace para restablecer la contraseña.
                        </div>
                    </div>

                    <div class="form-group no-margin">
                        <button type="submit" class="btn btn-primary btn-block">
                            Restablecer contraseña
                        </button>
                    </div>
                    <div class="margin-top20 text-center">
                        ¿Recuerdas la contraseña? <a href="{{url('/')}}">Login</a>
                    </div>
                </form>
            </div>
        </div>
@endsection
