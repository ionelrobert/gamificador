@extends('auth.layout.layout')
@section('content')
    <div class="card-wrapper">
        <div class="brand">
            <img src="{{url('profesores/assets/img/logo.png')}}">
        </div>
        <div class="card fat">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <div class="card-body">
                <h4 class="card-title">Restablecer Contraseña</h4>
                <form method="POST" method="POST" action="{{ route('password.request') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="token" value="{{ $token }}">
                    <div class="form-group">
                        <label for="email">E-Mail</label>

                        <input id="email" type="email" class="form-control" name="email" value="" required autofocus>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password">Nueva Contraseña</label>
                        <input id="password" type="password" class="form-control" name="password" required data-eye>
                        @if ($errors->has('password'))
                            <span class="help-block">
							<strong>{{ $errors->first('password') }}</strong>
						</span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="password-confirm">Confiramción Contraseña</label>
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required data-eye>

                    </div>

                    <div class="form-group no-margin">
                        <button type="submit" class="btn btn-primary btn-block">
                            Restablecer contraseña
                        </button>
                    </div>
                </form>
            </div>
        </div>
@endsection
