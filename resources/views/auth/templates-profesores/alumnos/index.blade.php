@extends('auth.profesores.layout')
@section('navbar-brand')
    Alumnos
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            @include('auth.partials.success-warning')
            @include('auth.partials.errores')
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="purple">
                    <!-- Trigger the modal with a button -->
                    <h4 class="title">Alumnos</h4>
                    <p class="category">Tabla de alumnos</p>
                </div>
                <div class="card-content table-responsive">
                    <table class="table" id="example">
                        <thead class="text-primary">
                            <th>Id Usuario</th>
                            <th>Nombre Completo</th>
                            <th>Email</th>
                            <th>Cursos</th>
                        </thead>
                        <tbody>
                            @foreach($alumnos as $alumno)
                                <tr>
                                    <td>{{$alumno->user_id}}</td>
                                    <td class="text-primary">{{$alumno->fullname}}</td>
                                    <td><a href="mailto:{{$alumno->user->email}}">{{$alumno->user->email}}</a></td>
                                    <td>
                                        @foreach($alumno->cursos()->get() as $curso)
                                            {{$curso->nombre}} - {{$curso->grupo}}<br>
                                        @endforeach
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    {!! Html::script('profesores/assets/js/roles.js') !!}
@endsection
