<!-- Modal -->
<div class="modal fade" id="newRolModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Nuevo Rol</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['route' => 'newRol', 'method' => 'post', 'role' => 'form', 'files' => false]) }}
                    {!! csrf_field() !!}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                {{ Form::label('name', 'Nombre:*') }}
                                {{ Form::text('name', old('name'), ['class' => 'form-control', "id"=>"name", 'required'=>true]) }}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                {{ Form::label('display_name', 'Nombre a mostrar:*') }}
                                {!! Form::text('display_name', old('display_name'), ['class' => 'form-control', "id"=>"display_name"]) !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                {{ Form::label('description', 'Descripción:*') }}
                                {!! Form::text('description', old('description'), ['class' => 'form-control', "id"=>"description"]) !!}
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('AÑADIR', ['class' => 'btn btn-success']) !!}
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {{ Form::close() }}
        </div>

    </div>
</div>