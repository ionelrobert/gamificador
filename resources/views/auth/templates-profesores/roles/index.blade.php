@extends('auth.profesores.layout')
@section('navbar-brand')
    Roles
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            @include('auth.partials.success-warning')
            @include('auth.partials.errores')
            @include('auth.templates-profesores.roles.partials.edit')
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="purple">
                    <!-- Trigger the modal with a button -->
                    <!--<button type="button"
                            class="btn btn-primary pull-right"
                            data-toggle="modal"
                            data-target="#newRolModal">
                            Nuevo Rol
                    </button>-->
                    <h4 class="title">Roles</h4>
                    <p class="category">Tabla de roles</p>
                </div>
                <div class="card-content table-responsive">
                    <table class="table" id="example">
                        <thead class="text-primary">
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Display Name</th>
                            <th>Descripción</th>
                            <th>Acciones</th>
                        </thead>
                        <tbody>
                            @foreach($roles as $rol)
                                <tr>
                                    <td>{{$rol->id}}</td>
                                    <td>{{$rol->name}}</td>
                                    <td class="text-primary">{{$rol->display_name}}</td>
                                    <td>{{$rol->description}}</td>
                                    <td>
                                        <a class="btn btn-default btn-raised legitRipple" onclick="edit({{$rol}})"
                                           data-toggle="tooltip" title="Editar Rol">
                                            <i class="fa fa-pencil"></i>
                                        </a>&nbsp;&nbsp;
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    {!! Html::script('profesores/assets/js/roles.js') !!}
@endsection
