<!-- Modal -->
<div class="modal fade" id="editModuloModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Editando: <strong id="name"></strong></h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['route' => 'editModulo', 'method' => 'post', 'role' => 'form', 'files' => false]) }}
                    {!! csrf_field() !!}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                {{ Form::hidden('id', old('id'), ['class' => 'form-control', "id"=>"id", 'required'=>true]) }}

                                {{ Form::label('nombre', 'Nombre:*') }}
                                {{ Form::text('nombre', old('nombre'), ['class' => 'form-control', "id"=>"nombre", 'required'=>true]) }}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                {{ Form::label('durada', 'Durada:*') }}
                                {!! Form::number('durada', old('durada'), ['class' => 'form-control', "id"=>"durada"]) !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                @if(count($profesores) > 0)
                                    <label for="profesor_id">Profesor:*</label><br>
                                    <select name="profesor_id" id="profesor_id" class="form-control">
                                        @foreach($profesores as $profesor)
                                            <option value="{{$profesor->user_id}}">{{$profesor->nombre}}</option>
                                        @endforeach
                                    </select>
                                @else
                                    <span class="form-control"><strong>No hay profesores . . .</strong></span>
                                    <input type="profesor_id" id="profesor_id" name="profesor_id" class="form-control" style="display: none;" required>
                                @endif
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                @if(count($profesores) > 0)
                    {!! Form::submit('EDITAR', ['class' => 'btn btn-success']) !!}
                @endif
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {{ Form::close() }}
        </div>

    </div>
</div>