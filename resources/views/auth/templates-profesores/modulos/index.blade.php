@extends('auth.profesores.layout')
@section('navbar-brand')
    Módulos
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            @include('auth.partials.success-warning')
            @include('auth.partials.errores')
            @include('auth.templates-profesores.modulos.partials.create')
            @include('auth.templates-profesores.modulos.partials.edit')
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="purple">
                    <button type="button"
                            class="btn btn-primary pull-right"
                            data-toggle="modal"
                            data-target="#newModuloModal">
                        Nuevo Módulo
                    </button>
                    <h4 class="title">Módulos</h4>
                    <p class="category">Tabla de módulos</p>
                </div>
                <div class="card-content table-responsive">
                    <table class="table" id="example">
                        <thead class="text-primary">
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Durada</th>
                            <th>Profesor</th>
                            <th>Acciones</th>
                        </thead>
                        <tbody>
                            @foreach($modulos as $modulo)
                                <tr>
                                    <td>{{$modulo->id}}</td>
                                    <td>{{$modulo->nombre}}</td>
                                    <td class="text-primary">{{$modulo->durada}}</td>
                                    <td>{{$modulo->profesor()->get()->first()['nombre']}}</td>
                                    <td>
                                        <a class="btn-raised legitRipple"
                                           onclick="edit({{$modulo}},{{$modulo->profesor()->get()}})"
                                           data-toggle="tooltip" title="Editar Módulo">
                                            <img src="{{url('iconos/editar.png')}}" class="img-action">
                                            <span class="sr-only">Editar</span>
                                        </a>
                                        <a class="buttonBorrar legitRipple"
                                           href="{{ route('deleteModulo',$modulo->id) }}"
                                           data-toggle="tooltip"
                                           title="Eliminar Módulo">
                                            <img src="{{url('iconos/borrar.png')}}" class="img-action">
                                            <span class="sr-only">Eliminar</span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    {!! Html::script('profesores/assets/js/modulos.js') !!}
@endsection
