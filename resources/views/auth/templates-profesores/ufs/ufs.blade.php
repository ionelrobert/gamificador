@extends('auth.profesores.layout')
@section('navbar-brand')
    UFS
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            @include('auth.partials.success-warning')
            @include('auth.partials.errores')
            @include('auth.templates-profesores.ufs.partials.create')
            @include('auth.templates-profesores.ufs.partials.edit')
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="purple">
                    <button type="button"
                            class="btn btn-primary pull-right"
                            data-toggle="modal"
                            data-target="#newUfModal">
                        Nueva UF
                    </button>
                    <h4 class="title">UFS</h4>
                    <p class="category">Tabla de UFS</p>
                </div>
                <div class="card-content table-responsive">
                    <table class="table" id="example">
                        <thead class="text-primary">
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Horas</th>
                            <th>Módulo</th>
                            <th>Alumnos</th>
                            <th>Acciones</th>
                        </thead>
                        <tbody>
                            @foreach($modulos as $modulo)
                                @foreach($modulo->ufs()->get() as $uf)
                                    <tr>
                                        <td>{{$uf->id}}</td>
                                        <td>{{$uf->nombre}}</td>
                                        <td>{{$uf->horas}}</td>
                                        <td>{{strip_tags(substr($modulo->nombre,0,3))}}</td>
                                        <td>
                                            {{count($uf->alumnos()->get())}}
                                        </td>
                                        <td>
                                            <a class="btn-raised legitRipple"
                                               onclick="edit({{$uf}}, {{$modulo}})"
                                               data-toggle="tooltip" title="Editar Módulo">
                                                <img src="{{url('iconos/editar.png')}}" class="img-action">
                                                <span class="sr-only">Editar</span>
                                            </a>
                                            <a class="buttonBorrar legitRipple"
                                               href="{{ route('deleteUf',$uf->id) }}"
                                               data-toggle="tooltip"
                                               title="Eliminar Módulo">
                                                <img src="{{url('iconos/borrar.png')}}" class="img-action">
                                                <span class="sr-only">Eliminar</span>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    {!! Html::script('profesores/assets/js/ufs.js') !!}
@endsection
