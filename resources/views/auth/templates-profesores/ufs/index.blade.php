@extends('auth.profesores.layout')
@section('navbar-brand')
    Módulos
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            @include('auth.partials.success-warning')
            @include('auth.partials.errores')

        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="purple">
                    <button type="button"
                            class="btn btn-primary pull-right"
                            data-toggle="modal"
                            data-target="#newModuloModal">
                        Nueva UF
                    </button>
                    <h4 class="title">UFS</h4>
                    <p class="category">Tabla de UFS</p>
                </div>
                <div class="card-content table-responsive">
                    <table class="table">
                        <thead class="text-primary">
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Horas</th>
                            <th>Módulo</th>
                            <th>Acciones</th>
                        </thead>
                        <tbody>
                            @foreach($ufs as $uf)
                                <tr>
                                    <td>{{$uf->id}}</td>
                                    <td>{{$uf->nombre}}</td>
                                    <td class="text-primary">{{$uf->horas}}</td>
                                    <td>{{$uf->modulo()->get()->first()->nombre}}</td>
                                    <td>
                                        <a class="btn btn-default btn-raised legitRipple"
                                           onclick="edit()"
                                           data-toggle="tooltip" title="Editar Módulo">
                                            <i class="fa fa-pencil" aria-hidden="true"></i>
                                            <span class="sr-only">Editar</span>
                                        </a>
                                        <a class="btn btn-danger buttonBorrar legitRipple"
                                           href="{{ route('deleteModulo',$uf->id) }}"
                                           data-toggle="tooltip"
                                           title="Eliminar Módulo">
                                            <i class="fa fa-user-times" aria-hidden="true"></i>
                                            <span class="sr-only">Eliminar</span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    {!! Html::script('profesores/assets/js/modulos.js') !!}
@endsection
