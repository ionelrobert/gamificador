@extends('auth.profesores.layout')
@section('navbar-brand')
    ¿De qué módulo?
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="card card-stats">
                <div class="card-header" data-background-color="orange">
                    <i class="fa fa-address-book-o" aria-hidden="true"></i>
                </div>
                <div class="card-content">
                    <p class="category">CFGS</p>
                    <h3 class="title">DAW</h3>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <a href="{{route('ufsDaw')}}" class="btn btn-info">Ver UFS</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="card card-stats">
                <div class="card-header" data-background-color="green">
                    <i class="fa fa-address-book-o" aria-hidden="true"></i>
                </div>
                <div class="card-content">
                    <p class="category">CFGS</p>
                    <h3 class="title">ASIX</h3>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <a href="{{route('ufsAsix')}}" class="btn btn-info">Ver UFS</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="card card-stats">
                <div class="card-header" data-background-color="red">
                    <i class="fa fa-address-book-o" aria-hidden="true"></i>
                </div>
                <div class="card-content">
                    <p class="category">CFGM</p>
                    <h3 class="title">SMX</h3>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <a href="{{route('ufsSmx')}}" class="btn btn-info">Ver UFS</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    {!! Html::script('profesores/assets/js/modulos.js') !!}
@endsection
