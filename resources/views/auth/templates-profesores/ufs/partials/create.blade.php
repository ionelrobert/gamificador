<!-- Modal -->
<div class="modal fade" id="newUfModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Nueva UF</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['route' => 'newUf', 'method' => 'post', 'role' => 'form', 'files' => false]) }}
                    {!! csrf_field() !!}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                {{ Form::label('nombre', 'Nombre:*') }}
                                {{ Form::text('nombre', old('nombre'), ['class' => 'form-control', "id"=>"nombre", 'required'=>true]) }}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                {{ Form::label('horas', 'Horas:*') }}
                                {!! Form::number('horas', old('horas'), ['class' => 'form-control', "id"=>"horas"]) !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="modulo_id">Escoje el módulo</label>
                                <select name="modulo_id" id="modulo_id" class="form-control">
                                    @foreach($modulos as $modulo)
                                        <option value="{{$modulo->id}}">{{$modulo->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="alumnos">Escoje los alumnos</label>
                                <select name="alumnos[]" id="alumnos[]" class="form-control" multiple>
                                    @foreach($alumnos as $alumno)
                                        @foreach($alumno->cursos()->get() as $curso)
                                            <option value="{{$alumno->user_id}}">{{$alumno->nombre}} {{$alumno->apellidos}} ({{$curso->nombre}})</option>
                                        @endforeach
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('AÑADIR', ['class' => 'btn btn-success']) !!}
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {{ Form::close() }}
        </div>

    </div>
</div>