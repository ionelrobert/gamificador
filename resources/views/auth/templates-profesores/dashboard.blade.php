@extends('auth.profesores.layout')
@section('navbar-brand')
    Dashboard {{Auth::user()->name}}
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            @include('auth.partials.success-warning')
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header" data-background-color="green">
                    <i class="fa fa-check" aria-hidden="true"></i>
                </div>
                <div class="card-content">
                    <p class="category">Unidades Formativas</p>
                    <h3 class="title counter-count">
                        {{cuantasUfs()}}
                    </h3>
                </div>
                <div class="card-footer text-center">
                    @if(modulosProfesor() != 0)
                        <a class="btn btn-success"  href="{{route('ufsProfesor')}}"><i class="fa fa-check" aria-hidden="true"></i> Calificar</a>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header" data-background-color="blue">
                    <i class="fa fa-users" aria-hidden="true"></i>
                </div>
                <div class="card-content">
                    <p class="category">Items</p>
                    <h3 class="title counter-count">{{cuantosItems()}}</h3>
                </div>
                <div class="card-footer text-center">
                        @if(modulosProfesor() != 0)
                            <a class="btn btn-info" href="{{route('itemsProfesor')}}"><i class="fa fa-eye" aria-hidden="true"></i> Mis Items</a>
                        @endif
                    </div>
                </div>
            </div>
        @role('superprofesor')
         <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card card-stats ">
                    <div class="card-header text-center" data-background-color="red">
                        <i class="fa fa-window-close-o" aria-hidden="true"></i>
                    </div>
                    <div class="card-content">
                        <p class="category">Alumnos</p>
                        <h3 class="title counter-count">{{cuantosAlumnos()}}</h3>
                    </div>
                    <div class="card-footer text-center">
                            <a href="{{route('resetCurso')}}" class="btn btn-danger btn-lg btn-reset"><i class="fa fa-window-close-o" aria-hidden="true"></i> Reset Curso</a>
                    </div>
                </div>
            </div>
         </div>
        @endrole
    </div>
@endsection
@section('scripts')
    {!! Html::script('profesores/assets/js/dashboard.js') !!}
    <script>
        $('.btn-reset').click(function(e) {
            e.preventDefault();
            var linkURL = $(this).attr("href");
            if(confirm("¿Seguro?")){
                window.location.href = linkURL;
            }
        });
    </script>
@endsection
