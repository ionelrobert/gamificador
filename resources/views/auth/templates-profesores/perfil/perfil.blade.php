@extends('auth.profesores.layout')
@section('navbar-brand')
    Mi perfil
@endsection
@section('content')
    <style>
        .btn-file {
            position: relative;
            overflow: hidden;
        }
        .btn-file input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            min-width: 100%;
            min-height: 100%;
            font-size: 100px;
            text-align: right;
            filter: alpha(opacity=0);
            opacity: 0;
            outline: none;
            background: white;
            cursor: inherit;
            display: block;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            @include('auth.partials.success-warning')
            @include('auth.partials.errores')
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="card card-profile">
                <div class="card-avatar">
                    <a href="" onclick="event.preventDefault();">
                        @if($usuario->imagen != null)
                            <img class="img" src="{{url('perfiles/profesores/'.$usuario->imagen)}}" />
                        @else
                            <img class="img" src="{{url('profesores/assets/img/faces/marc.jpg')}}" />
                        @endif
                    </a>
                </div>
                <div class="content">
                    <h6 class="category text-gray">
                        @foreach($usuario->roles()->get() as $rol)
                            {{$rol->name}}
                        @endforeach
                    </h6>
                    <h4 class="card-title">{{$usuario->name}}
                        @if($usuario->profesor)
                            {{$usuario->profesor->apellidos}}
                        @endif
                    </h4>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="purple">
                    <h4 class="title">Editar Prerfil</h4>
                    <p class="category">Completa la información de tu perfil</p>
                </div>
                <div class="card-content">
                    {{ Form::open(['url' => '/admin/mi-perfil/editar', 'method' => 'post', 'role' => 'form', 'files' => true]) }}
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label class="control-label">Dirección Email</label>
                                    <input type="email" id="email" name="email" class="form-control" value="{{$usuario->email}}" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Nombre</label>
                                    <input type="text" id="nombre" name="nombre" class="form-control" value="{{$usuario->name}}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Apellidos</label>
                                    @if($usuario->profesor)
                                        <input type="text" id="apellidos" name="apellidos" class="form-control" value="{{$usuario->profesor->apellidos}}">
                                    @else
                                        <input type="text" id="apellidos" name="apellidos" class="form-control">
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Contraseña</label>
                                    <input type="password" id="password" name="password" class="form-control" data-eye>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Confirmar contraseña</label>
                                    <input type="password" id="password-confirm" name="password_confirmation" class="form-control" data-eye>
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <span class="btn btn-info btn-file">
                                        Cambiar foto de perfil <input type="file" id="imagen" name="imagen">
                                    </span>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" id="id" name="id" class="form-control" value="{{$usuario->id}}" readonly="true" required>

                        {!! Form::submit('Actualizar Perfil', ['class' => 'btn btn-primary pull-right']) !!}
                        <div class="clearfix"></div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>

    </div>
@endsection
@section('scripts')

@endsection
