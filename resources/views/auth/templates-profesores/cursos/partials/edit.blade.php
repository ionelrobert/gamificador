<!-- Modal -->
<div class="modal fade" id="editCursoModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Editando: <strong id="name"></strong></h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['route' => 'editCurso', 'method' => 'post', 'role' => 'form', 'files' => false]) }}
                {!! csrf_field() !!}
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            {{ Form::hidden('id', old('id'), ['class' => 'form-control', "id"=>"id", 'required'=>true]) }}

                            {{ Form::label('nombre', 'Nombre:*') }}
                            {{ Form::text('nombre', old('nombre'), ['class' => 'form-control', "id"=>"nombre", 'required'=>true]) }}
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            {{ Form::label('grupo', 'Grupo:*') }}
                            {!! Form::text('grupo', old('grupo'), ['class' => 'form-control', "id"=>"grupo",'required'=>true]) !!}
                        </div>
                    </div>
                    <div class="col-md-12">
                        @if(count($alumnos) > 0)
                            <label for="alumnos">Alumnos:*</label><br>
                            <select name="alumnos[]" id="alumnos" class="form-control" multiple>
                                @foreach($alumnos as $alumnos)
                                    <option value="{{$alumnos->user_id}}" id="alumno">{{$alumnos->nombre}}</option>
                                @endforeach
                            </select>
                        @else
                            <span class="form-control"><strong>No hay alumnos . . .</strong></span>
                            <input type="text" id="alumnos" name="alumnos[]" class="form-control" style="display: none;" required>
                        @endif
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('EDITAR', ['class' => 'btn btn-success']) !!}
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {{ Form::close() }}
        </div>

    </div>
</div>