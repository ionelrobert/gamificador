@extends('auth.profesores.layout')
@section('navbar-brand')
    Cursos
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            @include('auth.partials.success-warning')
            @include('auth.partials.errores')
            @include('auth.templates-profesores.cursos.partials.create')
            @include('auth.templates-profesores.cursos.partials.edit')
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="purple">
                    <button type="button"
                            class="btn btn-primary pull-right"
                            data-toggle="modal"
                            data-target="#newCursoModal">
                        Nuevo Curso
                    </button>
                    <h4 class="title">Cursos</h4>
                    <p class="category">Tabla de cursos</p>
                </div>
                <div class="card-content table-responsive">
                    <table class="table" id="example">
                        <thead class="text-primary">
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Grupo</th>
                            <th>Alumnos</th>
                            <th>Acciones</th>
                        </thead>
                        <tbody>
                            @foreach($cursos as $curso)
                                <tr>
                                    <td>{{$curso->id}}</td>
                                    <td>{{$curso->nombre}}</td>
                                    <td class="text-primary">{{$curso->grupo}}</td>
                                    <td>
                                        {{count($curso->alumnos()->get())}}
                                    </td>
                                    <td>
                                        <a class="btn-raised legitRipple"
                                           onclick="edit({{$curso}})"
                                           data-toggle="tooltip" title="Editar Curso">
                                            <img src="{{url('iconos/editar.png')}}" style="width: 50px;">
                                            <span class="sr-only">Editar</span>
                                        </a>
                                        <a class="buttonBorrar legitRipple"
                                           href="{{ route('deleteCurso',$curso->id) }}"
                                           data-toggle="tooltip"
                                           title="Eliminar Curso">
                                            <!--<i class="fa fa-user-times" aria-hidden="true"></i>-->
                                            <img src="{{url('iconos/borrar.png')}}" style="width: 50px;">
                                            <span class="sr-only">Eliminar</span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    {!! Html::script('profesores/assets/js/cursos.js') !!}
@endsection
