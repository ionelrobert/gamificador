@extends('auth.profesores.layout')
@section('navbar-brand')
    Usuarios
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            @include('auth.partials.success-warning')
            @include('auth.partials.errores')
            @include('auth.templates-profesores.usuarios.partials.edit')
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="purple">
                    <h4 class="title">Usuarios</h4>
                    <p class="category">Tabla de usuarios</p>
                </div>
                <div class="card-content table-responsive">
                    <table class="table" id="example">
                        <thead class="text-primary">
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Email</th>
                            <th>Roles</th>
                            <th>Acciones</th>
                        </thead>
                        <tbody>
                            @foreach($usuarios as $usuario)
                                <tr>
                                    <td>{{$usuario->id}}</td>
                                    <td>{{$usuario->name}}</td>
                                    <td class="text-primary"><a href="mailto:{{$usuario->email}}">{{$usuario->email }}</a></td>
                                    <td>
                                        @if(count($usuario->roles()->get()) > 0)
                                            @foreach($usuario->roles()->get() as $rol)
                                                {{$rol->display_name}} <br>
                                            @endforeach
                                        @else
                                            No tiene roles asignados...
                                        @endif
                                    </td>
                                    <td>
                                        <a class="btn-raised legitRipple"
                                           onclick="edit({{$usuario}})"
                                           data-toggle="tooltip" title="Editar Usuario">
                                            <img src="{{url('iconos/editar.png')}}" class="img-action">
                                            <span class="sr-only">Editar</span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
@endsection
@section('scripts')
    {!! Html::script('profesores/assets/js/usuarios.js') !!}
@endsection
