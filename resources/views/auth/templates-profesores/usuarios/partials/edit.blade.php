<!-- Modal -->
<div class="modal fade" id="editUserModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Asignar roles al usuario: <strong><span id="email-user"></span></strong></h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['route' => 'editUser', 'method' => 'post', 'role' => 'form', 'files' => false]) }}
                    {!! csrf_field() !!}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" >
                                {{ Form::hidden('id', old('id'), ['class' => 'form-control', "id"=>"id", 'required'=>true]) }}
                                <label for="roles">Roles:*</label><br>
                                <select name="roles[]" id="roles" multiple style="width: 100% !important; height: 100% !important ">
                                    @foreach($roles as $role)
                                        <option value="{{$role->id}}">{{$role->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

            </div>
            <div class="modal-footer">
                {!! Form::submit('EDITAR', ['class' => 'btn btn-success']) !!}
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {{ Form::close() }}
        </div>

    </div>
</div>