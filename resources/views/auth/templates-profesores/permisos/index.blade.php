@extends('auth.profesores.layout')
@section('content')
    <div class="row">
        <div class="col-md-12">
            @include('auth.partials.success-warning')
            @include('auth.partials.errores')
            @include('auth.templates-profesores.permisos.partials.create')
            @include('auth.templates-profesores.permisos.partials.edit')
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="purple">
                    <button type="button"
                            class="btn btn-primary pull-right"
                            data-toggle="modal"
                            data-target="#newRolModal">
                        Nuevo Permiso
                    </button>
                    <h4 class="title">Permisos</h4>
                    <p class="category">Tabla de permisos</p>
                </div>
                <div class="card-content table-responsive">
                    <table class="table">
                        <thead class="text-primary">
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Display Name</th>
                            <th>Descripción</th>
                            <th>Roles</th>
                            <th>Acciones</th>
                        </thead>
                        <tbody>
                        @foreach($permisos as $permiso)
                            <tr>
                                <td>{{$permiso->id}}</td>
                                <td>{{$permiso->name}}</td>
                                <td class="text-primary">{{$permiso->display_name}}</td>
                                <td>{{$permiso->description}}</td>
                                <td>
                                    @if(count($permiso->roles()->get()) > 0)
                                        @foreach($permiso->roles()->get() as $rol)
                                            {{$rol->display_name}}
                                        @endforeach
                                    @else
                                        No tiene roles asignados...
                                    @endif
                                </td>
                                <td>
                                    <a class="btn btn-default btn-raised legitRipple" onclick="edit({{$permiso}})"
                                       data-toggle="tooltip" title="Editar Cliente">
                                        <i class="fa fa-pencil"></i>
                                    </a>&nbsp;&nbsp;
                                    <a class="btn btn-danger buttonBorrar legitRipple"
                                       href="{{ route('deletePermiso',$permiso->id) }}"
                                       data-toggle="tooltip"
                                       title="Eliminar Rol">
                                        <i class="fa fa-user-times"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    {!! Html::script('profesores/assets/js/permisos.js') !!}
@endsection
