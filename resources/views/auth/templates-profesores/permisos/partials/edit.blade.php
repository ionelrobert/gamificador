<!-- Modal -->
<div class="modal fade" id="editRolModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Editar Rol</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['route' => 'editRol', 'method' => 'post', 'role' => 'form', 'files' => false]) }}
                    {!! csrf_field() !!}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                {{ Form::hidden('id', old('id'), ['class' => 'form-control', "id"=>"id", 'required'=>true]) }}

                                {{ Form::label('name', 'Nombre:*') }}
                                {{ Form::text('name', old('name'), ['class' => 'form-control', "id"=>"name", 'required'=>true]) }}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                {{ Form::label('display_name', 'Nombre a mostrar:*') }}
                                {!! Form::text('display_name', old('display_name'), ['class' => 'form-control', "id"=>"display_name"]) !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                {{ Form::label('description', 'Descripción:*') }}
                                {!! Form::text('description', old('description'), ['class' => 'form-control', "id"=>"description"]) !!}
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('EDITAR', ['class' => 'btn btn-success']) !!}
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {{ Form::close() }}
        </div>

    </div>
</div>