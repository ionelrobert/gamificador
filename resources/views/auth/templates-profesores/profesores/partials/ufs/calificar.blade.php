<!-- Modal -->
<div class="modal fade" id="calificarModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Calificar Alumno</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['route' => 'buscarAlumno', 'method' => 'POST', 'role' => 'form', 'id'=>'form-nf-new']) }}
                    {!! csrf_field() !!}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group hidden">
                                {{ Form::hidden('uf_id', 'ID UF:') }}
                                {{ Form::hidden('uf_id', old('uf_id'), ['class' => 'form-control', "id"=>"uf_id", 'required'=>true]) }}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="alumno_id">Seleccione el Alumno</label>
                                <select name="alumno_id" id="alumno_id" class="form-control" required>
                                    <option value="" selected>Seleccione el Alumno</option>
                                    @if(modulosProfesor()!=0)
                                        @foreach(modulosProfesorObjeto()->get() as $modulo)
                                            @foreach($modulo->ufs()->get() as $uf)
                                                <optgroup label="{{$uf->nombre}}">
                                                @foreach($uf->alumnos()->get() as $alumno)
                                                        <option value="{{$alumno->user_id}}">
                                                            {{$alumno->nombre}} - {{$alumno->apellidos}}
                                                        </option>
                                                @endforeach
                                                </optgroup>
                                            @endforeach
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                @if(modulosProfesorObjeto())
                    {!! Form::submit('CALIFICAR', ['class' => 'btn btn-success']) !!}
                @endif
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {{ Form::close() }}
        </div>

    </div>
</div>