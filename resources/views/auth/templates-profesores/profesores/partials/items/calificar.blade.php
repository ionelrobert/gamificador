<!-- Modal -->
<div class="modal fade" id="calificarModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Calificando a <strong id="name"></strong></h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['route' => 'puntuarAlumno', 'method' => 'POST', 'role' => 'form', 'id'=>'calificar-form']) }}
                {!! csrf_field() !!}
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group hidden">
                            {{ Form::hidden('alumno_id', 'ID Alumno:') }}
                            {{ Form::hidden('alumno_id', old('alumno_id'), ['class' => 'form-control', "id"=>"alumno_id", 'required'=>true]) }}
                        </div>
                        <div class="form-group hidden">
                            {{ Form::hidden('item_id', 'ID Item:') }}
                            {{ Form::hidden
                            ('item_id', old('item_id'), ['class' => 'form-control', "id"=>"item_id", 'required'=>true]) }}
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            {{ Form::label('valoracion', 'Nota:*') }}
                            <input type="range" class="form-control" min="0" max="" id="valoracion" name="valoracion"
                                   oninput="salida.value=valoracion.value" required>
                            <output id="salida" name="salida" for="valoracion" class="text-center"></output>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                @if(modulosProfesorObjeto())
                    {!! Form::submit('CALIFICAR', ['class' => 'btn btn-success']) !!}
                @endif
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {{ Form::close() }}
        </div>

    </div>
</div>