<!-- Modal -->
<div class="modal fade" id="newItemModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Nuevo Item</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['route' => 'itemsProfesorNew', 'method' => 'post', 'role' => 'form', 'files' => false, 'id'=>'form-nf-new']) }}
                    {!! csrf_field() !!}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                {{ Form::label('nombre', 'Nombre:*') }}
                                {{ Form::text('nombre', old('nombre'), ['class' => 'form-control', "id"=>"nombre", 'required'=>true]) }}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                {{ Form::label('valorMaximo', 'Valor máximo:*') }}
                                {{ Form::number('valorMaximo', old('valorMaximo'), ['class' => 'form-control','min'=>'0','max'=>'100' ,"id"=>"valorMaximo", 'required'=>true]) }}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="examen">Examen?</label>
                                <select name="examen" id="examen" class="form-control">
                                    <option value="1">Si</option>
                                    <option value="0" selected>No</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="activo">Activo?</label>
                                <select name="activo" id="activo" class="form-control">
                                    <option value="1">Si</option>
                                    <option value="0" selected>No</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="nf_id">Seleccione el Nuclio Formativo</label>
                                <select name="nf_id" id="nf_id" class="form-control" required>
                                    <option value="" selected>Seleccione el Nuclio Formativo</option>
                                    @if(modulosProfesor()!=0)
                                        @foreach(modulosProfesorObjeto()->get() as $modulo)
                                            <optgroup label="{{$modulo->nombre}}">
                                            @foreach($modulo->ufs()->get() as $uf)
                                                @foreach($uf->nfs()->get() as $nf)
                                                        <option value="{{$nf->id}}">
                                                        {{$nf->nombre}}
                                                        </option>
                                                @endforeach
                                            @endforeach
                                            </optgroup>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                @if(modulosProfesorObjeto())
                    {!! Form::submit('AÑADIR', ['class' => 'btn btn-success']) !!}
                @endif
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {{ Form::close() }}
        </div>

    </div>
</div>