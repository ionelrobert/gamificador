<!-- Modal -->
<div class="modal fade" id="newNfModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Nuevo Nuclio Formativo</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['route' => 'nfsProfesorNew', 'method' => 'post', 'role' => 'form', 'files' => false, 'id'=>'form-nf-new']) }}
                    {!! csrf_field() !!}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                {{ Form::label('nombre', 'Nombre:*') }}
                                {{ Form::text('nombre', old('nombre'), ['class' => 'form-control', "id"=>"nombre", 'required'=>true]) }}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                {{ Form::label('peso', 'Peso sobre la Unidad Formativa:*') }}
                                {{ Form::number('peso', old('peso'), ['class' => 'form-control','min'=>'1','max'=>'100' ,"id"=>"peso", 'required'=>true]) }}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="uf_id">Seleccione la Unidad Formativa</label>
                                <select name="uf_id" id="uf_id" class="form-control">
                                    <option value="" selected>Seleccionar Unidad Formativa</option>
                                    @if(modulosProfesor()!=0)
                                        @foreach(modulosProfesorObjeto()->get() as $modulo)
                                            <optgroup label="{{$modulo->nombre}}">
                                                @foreach($modulo->ufs()->get() as $uf)
                                                    <option value="{{$uf->id}}">
                                                        {{$uf->nombre}}
                                                    </option>
                                                @endforeach
                                            </optgroup>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                @if(modulosProfesorObjeto())
                    {!! Form::submit('AÑADIR', ['class' => 'btn btn-success']) !!}
                @endif
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {{ Form::close() }}
        </div>

    </div>
</div>