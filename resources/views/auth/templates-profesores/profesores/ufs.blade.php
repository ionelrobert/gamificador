@extends('auth.profesores.layout')
@section('navbar-brand')
    UFS {{Auth::user()->name}}
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            @include('auth.partials.success-warning')
            @include('auth.partials.errores')
            @include('auth.templates-profesores.profesores.partials.ufs.calificar')
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="purple">
                    <h4 class="title">UFS</h4>
                    <p class="category">Tabla ufs ({{Auth::user()->name}})</p>
                </div>
                <div class="card-content table-responsive">
                    <table class="table" id="example">
                        <thead class="text-primary">
                        <th>Nombre</th>
                        <th>Horas</th>
                        <th>Módulo</th>
                        <th>Acciones</th>
                        </thead>
                        <tbody>
                        @if(modulosProfesor()!=0)
                            @foreach(modulosProfesorObjeto()->get() as $modulo)
                                @foreach($modulo->ufs()->get() as $uf)
                                    <tr>
                                        <td class="text-primary">{{$uf->nombre}}</td>
                                        <td>{{$uf->horas}}h</td>
                                        <td>{{strip_tags(substr($modulo->nombre,0,3))}}</td>
                                        <td>
                                            <a class="btn btn-default btn-raised legitRipple"
                                               onclick="calificar({{$uf}})"
                                               data-toggle="tooltip" title="Calificar">
                                                <i class="fa fa-cogs" aria-hidden="true"></i>
                                                <span class="sr-only">Calificar</span>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    {!! Html::script('profesores/assets/js/ufs.js') !!}
@endsection
