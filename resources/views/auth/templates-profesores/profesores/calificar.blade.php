@extends('auth.profesores.layout')
@section('navbar-brand')
    Volver al Dashboard
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            @include('auth.partials.success-warning')
            @include('auth.partials.errores')
            @include('auth.templates-profesores.profesores.partials.items.calificar')
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="purple">
                    <span class="pull-right">
                        <h4 class="title">{{$uf->modulo()->get()->first()->nombre}}</h4>
                        <p class="category">{{$uf->nombre}}</p>
                    </span>
                    <h6 class="title">{{$alumno->user()->get()->first()->name}}</h6>
                    <div class="clearfix"></div>
                </div>
                <div class="card-content table-responsive">
                    @foreach($uf->nfs()->get() as $nf)
                        <hr>
                        <h4 class="title text-center">{{$nf->nombre}} ({{$nf->peso}}%)</h4>
                        <hr>
                        <div class="row">
                            @foreach($nf->items()->get() as $item)
                                <div class="col-md-3">
                                    <div class="thumbnail"  style="margin: 1px auto;">
                                        <img src="{{url('items/'.randomimg()[0])}}" alt="" class="img-circle img-responsive img-width">
                                        <div class="caption col-centered">
                                            <h3>{{$item->nombre}} ({{$item->valorMaximo}}%)</h3>
                                            @if(!$item->contains($item->id, $alumno->user_id))
                                                <div class="progress">
                                                    <div class="progress-bar progress-bar-striped active"
                                                         role="progressbar"
                                                         aria-valuenow="40"
                                                         aria-valuemin="0"
                                                         aria-valuemax="50"
                                                         style="width:{{$item->valorMaximo}}%;color: black;"
                                                         id="nota-{{$item->id}}">
                                                        {{$item->valorMaximo}}%
                                                    </div>
                                                </div>
                                            @else
                                                @foreach($alumno->items()->get() as $itemAl)
                                                    @if($itemAl->id == $item->id)
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-striped active"
                                                                 role="progressbar"
                                                                 aria-valuenow="40"
                                                                 aria-valuemin="0"
                                                                 aria-valuemax="50"
                                                                 style="width:{{$itemAl->pivot->valoracion}}%;color: black;"
                                                                 id="nota-{{$item->id}}">
                                                                {{$itemAl->pivot->valoracion}}%
                                                            </div>
                                                        </div>
                                                        Nota: <span id="notaa-{{$item->id}}">{{($itemAl->pivot->nota)}}</span>
                                                @endif
                                            @endforeach
                                            @endif
                                            <!--
                                            @if(count($alumno->items()->get())>0)
                                                @foreach($alumno->items()->get() as $itemAl)
                                                    @if($itemAl->id == $item->id)
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-striped active"
                                                                 role="progressbar"
                                                                 aria-valuenow="40"
                                                                 aria-valuemin="0"
                                                                 aria-valuemax="50"
                                                                 style="width:{{$itemAl->pivot->valoracion}}%;color: black;"
                                                                 id="nota-{{$item->id}}">
                                                                {{$itemAl->pivot->valoracion}}%
                                                            </div>
                                                        </div>
                                                        Nota: <span id="notaa-{{$item->id}}">{{($itemAl->pivot->nota)}}</span>
                                                    @endif
                                                @endforeach
                                            @endif
                                            -->
                                            <div class="form-group" id="valorar">
                                                @if($item->valorMaximo != 0)
                                                <input type="range" class="form-control" min="0" max="{{$item->valorMaximo}}" id="valoracion" name="valoracion"
                                                       onchange="calificarItem({{$item}},
                                                       {{$alumno->user()->get()->first()}},
                                                       {{$item->id}})">
                                                <output name="salidaValoracion-{{$item->id}}" id="salidaValoracion-{{$item->id}}" class="text-center">0%</output>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <br><br><br>
                        <div class="clearfix"></div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    {!! Html::script('profesores/assets/js/items.js') !!}
    <script>
        function calificarItem(item, alumno,id_button){
            $('.card-content #valorar #valoracion-'+id_button).attr({
                "max" : item['valorMaximo'],
                "min" : 0
            });

            $('#calificarModal #name').html(item['nombre']);
            $('#calificarModal #item_id').val(item['id']);
            $('#calificarModal #alumno_id').val(alumno['id']);


            $.ajax({
                url: "{{ url('/admin/profe/puntuar/') }}",
                method: 'POST',
                dataType: 'JSON',
                data: $('#calificar-form').serialize(),
                success: function(result){
                    $('#nota-'+id_button).css('width',result['valoracion']+'%');
                    $('#nota-'+id_button).html(result['valoracion']+'%');
                    $('#notaa-'+id_button).html(result['nota']);
                    $('#salidaValoracion-'+id_button).val(result['valoracion']+'%');
                },
                fail: function (result) {
                    console.log("fail");
                }
            });
        }
    </script>
@endsection
