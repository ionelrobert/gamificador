@extends('auth.profesores.layout')
@section('navbar-brand')
    NFS {{\Auth::user()->name}}
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            @include('auth.partials.success-warning')
            @include('auth.partials.errores')
            @include('auth.templates-profesores.profesores.partials.items.create')
            @include('auth.templates-profesores.profesores.partials.items.edit')
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="purple">
                    <button type="button"
                            class="btn btn-primary pull-right"
                            data-toggle="modal"
                            data-target="#newItemModal">
                        Nuevo Item
                    </button>
                    <h4 class="title">Items</h4>
                    <p class="category">Tabla items ({{\Auth::user()->name}})</p>
                </div>
                <div class="card-content table-responsive">
                    <table class="table" id="example">
                        <thead class="text-primary">
                        <th>Nombre</th>
                        <th>Valor Máximo</th>
                        <th>Es examen?</th>
                        <th>Activo</th>
                        <th>NF</th>
                        <th>UF</th>
                        <th>Acciones</th>
                        </thead>
                        <tbody>
                        @if(modulosProfesor()!=0)
                            @foreach(modulosProfesorObjeto()->get() as $modulo)
                                @foreach($modulo->ufs()->get() as $uf)
                                    @foreach($uf->nfs()->get() as $nf)
                                        @foreach($nf->items()->get() as $item)
                                            <tr>
                                                <td class="text-primary">{{$item->nombre}}</td>
                                                <td>{{$item->valorMaximo}}%</td>
                                                <td>{{$item->examen ? 'Si' : 'No'}}</td>
                                                <td>{{$item->activo ? 'Si' : 'No'}}</td>
                                                <td>{{$item->nf->nombre}}</td>
                                                <td>{{strip_tags(substr($uf->nombre,0,3))}}: {{$modulo->nombre}}</td>
                                                <td>
                                                    <a class="btn-raised legitRipple"
                                                       onclick="edit({{$item}},{{$uf}},{{$modulo}},{{$nf}})"
                                                       data-toggle="tooltip" title="Editar Item">
                                                        <img src="{{url('iconos/editar.png')}}" class="img-action">
                                                        <span class="sr-only">Editar</span>
                                                    </a>
                                                    <a class="buttonBorrar legitRipple"
                                                       href="{{ route('itemsProfesorDelete',$item->id) }}"
                                                       data-toggle="tooltip"
                                                       title="Eliminar Item">
                                                        <img src="{{url('iconos/borrar.png')}}" class="img-action">
                                                        <span class="sr-only">Eliminar</span>
                                                    </a>
                                                </td>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endforeach
                                @endforeach
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    {!! Html::script('profesores/assets/js/items.js') !!}
@endsection
