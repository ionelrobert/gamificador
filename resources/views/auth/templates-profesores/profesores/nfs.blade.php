@extends('auth.profesores.layout')
@section('navbar-brand')
    NFS {{\Auth::user()->name}}
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            @include('auth.partials.success-warning')
            @include('auth.partials.errores')
            @include('auth.templates-profesores.profesores.partials.nfs.create')
            @include('auth.templates-profesores.profesores.partials.nfs.edit')
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="purple">
                    <button type="button"
                            class="btn btn-primary pull-right"
                            data-toggle="modal"
                            data-target="#newNfModal">
                        Nuevo NF
                    </button>
                    <h4 class="title">NFS</h4>
                    <p class="category">Tabla nfs ({{\Auth::user()->name}})</p>
                </div>
                <div class="card-content table-responsive">
                    <table class="table" id="example">
                        <thead class="text-primary">
                        <th>Nombre</th>
                        <th>Peso</th>
                        <th>UF</th>
                        <th>Módulo</th>
                        <th>Acciones</th>
                        </thead>
                        <tbody>
                            @if(modulosProfesor()!=0)
                            @foreach(modulosProfesorObjeto()->get() as $modulo)
                                @foreach($modulo->ufs()->get() as $uf)
                                    @foreach($uf->nfs()->get() as $nf)
                                        <tr>
                                            <td class="text-primary">{{$nf->nombre}}</td>
                                            <td>{{$nf->peso}}%</td>
                                            <td>{{$uf->nombre}}</td>
                                            <td>{{$modulo->nombre}}</td>
                                            <td>
                                                <a class="btn-raised legitRipple"
                                                   onclick="edit({{$nf}},{{$uf}})"
                                                   data-toggle="tooltip" title="Editar NNFF">
                                                    <img src="{{url('iconos/editar.png')}}" class="img-action">
                                                    <span class="sr-only">Editar</span>
                                                </a>
                                                <a class="buttonBorrar legitRipple"
                                                   href="{{ route('nfsProfesordelete',$nf->id) }}"
                                                   data-toggle="tooltip"
                                                   title="Eliminar NNFF">
                                                    <img src="{{url('iconos/borrar.png')}}" class="img-action">
                                                    <span class="sr-only">Eliminar</span>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endforeach
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    {!! Html::script('profesores/assets/js/nfs.js') !!}
@endsection
