@extends('auth.profesores.layout')
@section('navbar-brand')
    Módulos {{\Auth::user()->name}}
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="purple">
                    <h4 class="title">Módulos</h4>
                    <p class="category">Tabla módulos ({{\Auth::user()->name}})</p>
                </div>
                <div class="card-content table-responsive">
                    <table class="table" id="example">
                        <thead class="text-primary">
                        <th>Nombre</th>
                        <th>Durada</th>
                        </thead>
                        <tbody>
                            @if(modulosProfesor()!=0)
                                @foreach(modulosProfesorObjeto()->get() as $modulo)
                                    <tr>
                                        <td class="text-primary">{{$modulo->nombre}}</td>
                                        <td>{{$modulo->durada}}h</td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')

@endsection
