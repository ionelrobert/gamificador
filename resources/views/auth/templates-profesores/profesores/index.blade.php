@extends('auth.profesores.layout')
@section('navbar-brand')
    Profesores
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            @include('auth.partials.success-warning')
            @include('auth.partials.errores')
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="purple">
                    <!-- Trigger the modal with a button -->
                    <h4 class="title">Profesores</h4>
                    <p class="category">Tabla de profesores</p>
                </div>
                <div class="card-content table-responsive">
                    <table class="table" id="example">
                        <thead class="text-primary">
                            <th>Id Usuario</th>
                            <th>Nombre Completo</th>
                            <th>Email</th>
                            <th>Cursos</th>
                        </thead>
                        <tbody>
                            @foreach($profesores as $profesor)
                                <tr>
                                    <td>{{$profesor->user_id}}</td>
                                    <td class="text-primary">{{$profesor->fullname}}</td>
                                    <td><a href="mailto:{{$profesor->user->email}}">{{$profesor->user->email}}</a></td>
                                    <td>
                                        @foreach($profesor->modulos()->get() as $modulo)
                                            {{$modulo->nombre}}<br>
                                        @endforeach
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    {!! Html::script('profesores/assets/js/roles.js') !!}
@endsection
