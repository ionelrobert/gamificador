@if (! $errors->isEmpty())
    <div class="pad margin no-print">
        <div class="alert alert-danger" style="margin-bottom: 0!important;">
            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Cerrar</span></button>
            <p><i class="fa fa-info"></i> <strong>Oops!</strong> Por favor corrige los siguientes errores:</p>
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif