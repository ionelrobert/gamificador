@if( Session::has( 'success' ))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Cerrar</span></button>
        <span class="text-bold">Bien Hecho!! </span> {!! session('success') !!}
    </div>
@elseif( Session::has( 'warning' ))
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Cerrar</span></button>
        <span class="text-bold">Error!! </span> {!! session('warning') !!}
    </div>
@endif