<meta charset="utf-8">
<meta name="author" content="Kodinger">
<title>Gamificador</title>
<link rel="icon" type="image/png" href="{{asset('profesores/assets/img/logo.png')}}" />
{{Html::style('vendor/login/bootstrap/css/bootstrap.min.css')}}
{{Html::style('vendor/login/css/my-login.css')}}

{{Html::style('alumnos/assets/css/bootstrap.min.css')}}
{{Html::style('alumnos/assets/css/mdb.min.css')}}
{{Html::style('alumnos/assets/css/compiled.min.css')}}
{{Html::style('https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons')}}
{{Html::style('https://use.fontawesome.com/releases/v5.0.11/css/all.css')}}
{{Html::style('alumnos/assets/css/alumnos.css')}}
