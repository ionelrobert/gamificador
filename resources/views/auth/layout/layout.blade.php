<!doctype html>
<html>
<head>
    @include('auth.layout.head')
</head>
<body class="my-login-page">
<div class="fondo">
<section class="h-100">
    <div class="container h-100">
        <div class="row justify-content-md-center h-100">
            <div class="card-wrapper">
                @yield('content')
            </div>
        </div>
    </div>
</section>
</div>
@include('auth.layout.scripts')
</body>
</html>