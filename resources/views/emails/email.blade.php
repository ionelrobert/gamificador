<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Material Design Bootstrap</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="http://gamificador.proendata.com/alumnos/assets/css/boostrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="http://gamificador.proendata.com/alumnos/assets/css/complied.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
</head>

<body>
<div class="container-fluid">
    <div class="card text-center" style="width: 22rem;">
        <div class="card-header rgba-red-strong white-text">
            <img src="http://gamificador.proendata.com/alumnos/assets/img/gamir.png" alt="logo" width="90" height="50">
        </div>
        <div class="card-body">
            <h4 class="card-title">Hola, {{$alumno->nombre}}</h4>
            <p class="card-text">No te has conectado hoy al gamificador</p>
            <p class="card-text">Tu salud ha bajado al {{$alumno->salud}}%</p>
            <a href="{{url('/login')}}" class="btn btn-success btn-sm">Gamificador</a>
        </div>
        <div class="card-footer text-muted grey-color text-white-50">
            <p class="mb-0">Realizado Ionel-Robert Duca & Marc Iniesta</p>
        </div>
    </div>
</div>

</body>
</html>