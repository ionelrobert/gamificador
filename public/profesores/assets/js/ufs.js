function edit(uf, modulo){
    $('#editUfModal #name').html(uf['nombre']);
    $('#editUfModal #nombre').val(uf['nombre']);
    $('#editUfModal #horas').val(uf['horas']);
    $('#editUfModal #id').val(uf['id']);
    $('#editUfModal #modulo_id').val(modulo['id']);
    $('#editUfModal').modal('show');
}

function calificar(uf){
    $('#calificarModal #uf_id').val(uf['id']);
    $('#calificarModal').modal('show');
}