function edit(curso){

    $('#editCursoModal #name').html(curso['nombre'] + " - " + curso['grupo']);
    $('#editCursoModal #nombre').val(curso['nombre']);
    $('#editCursoModal #grupo').val(curso['grupo']);
    $('#editCursoModal #id').val(curso['id']);
    $('#editCursoModal #alumno').change();
    $('#editCursoModal').modal('show');
}