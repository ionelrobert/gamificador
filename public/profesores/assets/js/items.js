function edit(item,uf, modulo,nf){
    $('#editItemModal #name').html(item['nombre'] + ' ('+modulo.nombre.substring(0,3)+'-'+ uf.nombre.substring(0,3) +')');
    $('#editItemModal #id').val(item['id']);
    $('#editItemModal #nombre').val(item['nombre']);
    $('#editItemModal #valorMaximo').val(item['valorMaximo']);
    $('#editItemModal #examen').val(item['examen']);
    $('#editItemModal #activo').val(item['activo']);
    $('#editItemModal #nf_id').val(nf['id']);
    $('#editItemModal').modal('show');
}
/*function calificarItem(item, alumno){
    $('#calificarModal #name').html(item['nombre']);
    $('#calificarModal #item_id').val(item['id']);
    $('#calificarModal #alumno_id').val(alumno['id']);

    $("#calificarModal #valoracion").attr({
        "max" : item['valorMaximo'],
        "min" : 0
    });
    $('#calificarModal').modal('show');
}*/

$('.card-content #valorar #valoracion').on('input', function () {
    $("#calificarModal #valoracion").val($(this).val());
});