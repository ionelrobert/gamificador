$('.buttonBorrar').click(function(e) {
    e.preventDefault();
    var linkURL = $(this).attr("href");
    if(confirm("¿Seguro?")){
        window.location.href = linkURL;
    }
});