function edit(modulo, profesor){
    $('#editModuloModal #name').html(modulo['nombre']);
    $('#editModuloModal #nombre').val(modulo['nombre']);
    $('#editModuloModal #durada').val(modulo['durada']);
    $('#editModuloModal #id').val(modulo['id']);
    if(profesor[0]){
        $('#editModuloModal #profesor_id').val(profesor[0].user_id);
    }
    $('#editModuloModal').modal('show');
}