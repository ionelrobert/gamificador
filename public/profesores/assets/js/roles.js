function edit(rol){
    $('#editRolModal #nombre').html(rol['name']);
    $('#editRolModal #name').val(rol['name']);
    $('#editRolModal #display_name').val(rol['display_name']);
    $('#editRolModal #description').val(rol['description']);
    $('#editRolModal #id').val(rol['id']);
    $('#editRolModal').modal('show');
}