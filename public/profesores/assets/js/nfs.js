function edit(nf,uf){

    $('#editNfModal #name').html(nf['nombre']);
    $('#editNfModal #id').val(nf['id']);
    $('#editNfModal #nombre').val(nf['nombre']);
    $('#editNfModal #peso').val(nf['peso']);
    $('#editNfModal #uf_id').val(uf['id']);
    $('#editNfModal').modal('show');
}