<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Item;
use App\Modulo;
use App\UF;
use App\User;
use Illuminate\Http\Request;

Route::get('/', function () {
    return redirect('/login ');
});


Route::get('/home', 'HomeController@index')->name('home');


Route::group(['before' => ['auth']],function() {
    Auth::routes();
    Route::get('/usuarios/confirmacion-identidad', 'ConfirmarIdentidadController@index');
    Route::post('/usuarios/confirmacion-identidad', 'ConfirmarIdentidadController@comprobarIdentidad')->name('confirmarIdentidad');
    Route::group(['middleware' => ['role:superprofesor']], function() {
        //Rutas roles --> SOLO superprofesor
        Route::get('/admin/roles', 'RolesController@index')->name('roles');

        //Rutas roles --> SOLO superprofesor
        Route::get('/admin/modulos', 'ModulosController@index')->name('modulos');
        Route::get('/admin/modulos/delete/{id}', 'ModulosController@delete')->name('deleteModulo');
        Route::post('/admin/modulos/new', 'ModulosController@create')->name('newModulo');
        Route::post('/admin/modulos/edit', 'ModulosController@edit')->name('editModulo');

        //Rutas profesores --> SOLO superprofesor
        Route::get('/admin/profesores', 'ProfesoresController@index')->name('profesores');

        //Rutas alumnos --> SOLO superprofesor
        Route::get('/admin/alumnos', 'AlumnosController@index')->name('alumnos');

        //Rutas cursos --> SOLO superprofesor
        Route::get('/admin/cursos', 'CursosController@index')->name('cursos');
        Route::get('/admin/cursos/delete/{id}', 'CursosController@delete')->name('deleteCurso');
        Route::post('/admin/cursos/new', 'CursosController@create')->name('newCurso');
        Route::post('/admin/cursos/edit', 'CursosController@edit')->name('editCurso');

        //Rutas ufs --> SOLO superprofesor
        Route::get('/admin/ufs', 'UfsController@index')->name('ufs');
        Route::get('/admin/ufs/daw', 'UfsController@dawUfs')->name('ufsDaw');
        Route::get('/admin/ufs/asix', 'UfsController@asixUfs')->name('ufsAsix');
        Route::get('/admin/ufs/smx', 'UfsController@smxUfs')->name('ufsSmx');
        Route::post('/admin/ufs/new', 'UfsController@create')->name('newUf');
        Route::post('/admin/ufs/edit', 'UfsController@edit')->name('editUf');
        Route::get('/admin/ufs/delete/{id}', 'UfsController@delete')->name('deleteUf');

        //Rutas usuarios --> algunas son SOLO para el superprofesor
        Route::get('/admin/usuarios', 'UsersController@index')->name('usuarios');
        Route::post('/admin/usuarios/edit', 'UsersController@edit')->name('editUser');
        Route::get('/admin/reset', 'CursosController@reset')->name('resetCurso');
    });

    Route::group(['middleware' => ['role:superprofesor|profe']], function() {
        //SÓLO Profesores y superprofesores
        Route::get('/admin/dashboard', 'UsersController@dashboardProfesores')->name('dashboardProfesores');

        Route::get('/admin/profe/modulos', 'ProfesoresController@modulos')->name('modulosProfesor');
        Route::get('/admin/profe/ufs', 'ProfesoresController@ufs')->name('ufsProfesor');
        Route::get('/admin/profe/nfs', 'ProfesoresController@nfs')->name('nfsProfesor');
        Route::post('/admin/profe/nfs/new', 'ProfesoresController@create')->name('nfsProfesorNew');
        Route::post('/admin/profe/nfs/edit', 'ProfesoresController@edit')->name('nfsProfesorEdit');
        Route::get('/admin/profe/nfs/delete/{id}', 'ProfesoresController@deleteNfs')->name('nfsProfesordelete');

        Route::get('/admin/profe/items', 'ProfesoresController@items')->name('itemsProfesor');
        Route::post('/admin/profe/items/new', 'ProfesoresController@createItem')->name('itemsProfesorNew');
        Route::post('/admin/profe/items/edit', 'ProfesoresController@editItem')->name('itemsProfesorEdit');
        Route::get('/admin/profe/items/delete/{id}', 'ProfesoresController@deleteItem')->name('itemsProfesorDelete');
        Route::post('/admin/profe/puntuarAlumno/', 'ProfesoresController@buscarAlumno')->name('buscarAlumno');
        //Route::post('/admin/profe/puntuar/', 'ProfesoresController@puntuarAlumno')->name('puntuarAlumno');

        //Peril profesores
        Route::get('/admin/mi-perfil/', 'UserProfileController@indexProfesor')->name('perfilProfesor');
        Route::post('/admin/mi-perfil/editar', 'UserProfileController@editPerfilProfesor')->name('editPerfilProfesor');

        Route::post('/admin/profe/puntuar/', function (Request $request){

            $item = Item::find($request->item_id);
            $nota = round( $request->valoracion * 10 / $item->valorMaximo, 1, PHP_ROUND_HALF_ODD);
            $item->alumnos()->detach($request->alumno_id);
            $item->alumnos()->attach([
                $request->alumno_id => ['valoracion' => $request->valoracion,'nota'=>$nota],
            ]);
            if($item->save()){
                return response()->json(["valoracion"=>$request->valoracion,"nota"=>$nota]);
            }else{
                $msg = "no guardado";
                return response()->json($msg);
            }
        })->name('puntuarAlumno');
    });
    Route::group(['middleware' => ['role:alumno']], function() {
        Route::get('/alumnos/dashboard', 'AlumnosController@dashboardAlumnos')->name('dashboardAlumnos');
        Route::get('/alumnos/detallesUf/{id}', 'AlumnosController@detallesUf')->name('detallesUf');
        Route::get('/alumnos/miperfil', 'AlumnosController@perfil')->name('perfilAlumno');
        Route::post('/alumnos/miperfil/editar', 'AlumnosController@editarPerfil')->name('editarPerfilAlumno');
    });
});
