<?php

use App\Curso;
use Illuminate\Database\Seeder;

class CursosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cursos = array(
            array('DAW', 'A'),
            array('DAW', 'B'),
            array('ASIX', 'A'),
            array('ASIX', 'B'),
            array('SMX', 'A'),
            array('SMX', 'B'),
        );

        foreach ($cursos as $curso){
            Curso::create([
                'nombre'    =>  $curso[0],
                'grupo'    =>  $curso[1],
            ]);
        }
    }
}
