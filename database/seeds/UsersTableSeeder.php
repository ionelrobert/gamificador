<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superprofe = Role::where('name', 'superprofesor')->get();
        $user = User::create([
            'name' => 'Admin',
            'email' => 'superprofesor@gmail.com',
            'password' => bcrypt('superprofesor'),
        ]);
        $user->roles()->sync($superprofe);
        $user->save();
    }
}
