<?php

use App\Modulo;
use App\UF;
use Illuminate\Database\Seeder;

class ModulosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mdaws = array(
            array('M01 - Sistemes informàtics (CFGS - DAW)', 198,
                    array(
                        array('UF1 - Instal·lació, configuració i explotació del sistema informàtic', 60),
                        array('UF2 - Gestió de la informació i de recursos en una xarxa', 80),
                        array('UF3 - Implantació de programari específic', 25)
                    )
                 ),
            array('M02 - Bases de dades (CFGS - DAW)', 231,
                    array(
                        array('UF1. Introducció a les bases de dades', 33),
                        array('UF2. Llenguatges SQL: DML i DDL', 66),
                        array('UF3. Llenguatge SQL: DCL i extensió procedimental', 66),
                        array('UF4. Bases de dades objecte-relacionals', 33)
                    )
                 ),
            array('M03 - Programació (CFGS - DAW)', 297,
                array(
                    array('UF1. Programació estructurada.', 85),
                    array('UF2. Disseny modular', 50),
                    array('UF3. Fonaments de gestió de fitxers', 30),
                    array('UF4. Propramació orientada a objectes. Fonaments', 35),
                    array('UF5. POO. Llibreries de classes fonamentals', 35),
                    array('UF6. POO. Introducció a la persistència en BD', 29)
                )
            ),
            array('M04 - Llenguatge de marques i sistemes de gestió d’informació (CFGS - DAW)', 99,
                array(
                    array('UF1. Programació amb XML', 45),
                    array('UF2. Àmbits d’aplicació de l’XML', 27),
                    array('UF3. Sistemes de gestió d’informació empresarial', 27)
                )
            ),
            array('M05 - Entorns de desenvolupament (CFGS - DAW)', 66,
                array(
                    array('UF1. Desenvolupament de programari', 20),
                    array('UF2. Optimització del programari', 20),
                    array('UF3. Introducció al disseny orientat a objectes', 26)
                )
            ),
            array('M06 - Desenvolupament web en entorn client (CFGS - DAW)', 165,
                array(
                    array('UF1. Sintaxi del llenguatge. Objectes predefinits del llenguatge.', 30),
                    array('UF2. Estructures definides pel programador. Objectes.', 30),
                    array('UF3. Esdeveniments. Manegament de formularis. Model d\'objectes del document.', 42),
                    array('UF4. Comunicació asíncrona client-servidor. ', 30)
                )
            ),
            array('M07 - Desenvolupament web en entorn servidor (CFGS - DAW)', 165,
                array(
                    array('UF1. Desenvolupament web en entorn servidor.', 30),
                    array('UF2. Generació dinàmica de pagines web.', 30),
                    array('UF3. Accés a dades.', 30),
                    array('UF4. Serveis web. Pàgines dinàmiques interactives. Webs Híbrides.', 42)
                )
            ),
            array('M08 - Desplegament d’aplicacions web (CFGS - DAW)', 99,
                array(
                    array('UF1. Servidors web i de transferència de fitxers', 39),
                    array('UF2. Servidors d’aplicacions web', 20),
                    array('UF3. Desplegament d’aplicacions web', 20),
                    array('UF4. Control de versions i documentació', 20)
                )
            ),
            array('M09 - Disseny d’interfícies web (CFGS - DAW)', 99,
                array(
                    array('UF1. Disseny de l\'interfície. Estils.', 39),
                    array('UF2. Elements multimèdia: creació i integració.', 30),
                    array('UF3. Accessibilitat i usabilitat', 30)
                )
            ),
        );

        foreach ($mdaws as $mdaw){
            $modulo = Modulo::create([
                'nombre'    =>  $mdaw[0],
                'durada'    =>  $mdaw[1],
            ]);
            foreach ($mdaw[2] as $muf){
                $uf[] = UF::create([
                    'nombre'    => $muf[0],
                    'horas'    => $muf[1],
                ]);
            }
            $modulo->ufs()->saveMany($uf);
            $uf = [];
        }
        $masixs = array(
            array('M01 - Implantació de sistemes operatius (CFGS - ASIX)', 231,
                    array(
                        array('UF1 - Instal·lació, configuració i explotació del sistema informàtic', 60),
                        array('UF2 - Gestió de la informació i de recursos en una xarxa', 80),
                        array('UF3 - Implantació de programari específic', 25),
                        array('UF4 - Seguretat, rendiment i recursos', 25)
                    )
                ),
            array('M02 - Gestió de bases de dades (CFGS - ASIX)', 165,
                array(
                    array('UF1. Introducció a les bases de dades', 33),
                    array('UF2. Llenguatges SQL: DML i DDL', 66),
                    array('UF3. Assegurament de la informació', 33)
                )
            ),
            array('M03 - Programació bàsica (CFGS - ASIX)', 165,
                array(
                    array('UF1. Programació estructurada.', 85),
                    array('UF2. Disseny modular', 50),
                    array('UF3. Fonaments de gestió de fitxers', 30),
                )
            ),
            array('M04 - Llenguatge de marques (CFGS - ASIX)', 99,
                array(
                    array('UF1. Programació amb XML', 45),
                    array('UF2. Àmbits d’aplicació de l’XML', 27),
                    array('UF3. Sistemes de gestió d’informació empresarial', 27),
                )
            ),
            array('M05 - Fonaments de maquinari (CFGS - ASIX)', 99,
                array(
                    array('UF1. Arquitectura de sistemes', 22),
                    array('UF2. Instal•lació, configuració i recuperació de programari', 22),
                    array('UF3. Implantació i manteniment de CPD', 22),
                )
            ),
            array('M06 - Administració de Sistemes Operatius (CFGS - ASIX)', 132,
                array(
                    array('UF1: Administració avançada de sistemes operatius', 70),
                    array('UF2:Automatització de tasques i llenguatges de guions', 29)
                )
            ),
            array('M07 - Planificació i administració de xarxes (CFGS - ASIX)', 165,
                array(
                    array('UF1. Introducció a les xarxes', 44),
                    array('UF2. Administració de dispositius de xarxa', 44),
                    array('UF3. Administració avançada de xarxes', 44)
                )
            ),
            array('M08 - Serveis de xarxa i Internet (CFGS - ASIX)', 99,
                array(
                    array('UF1. Serveis de noms i configuració automàtica', 25),
                    array('UF2. Serveis Web i de transferència de fitxers', 25),
                    array('UF3. Correu electrònic i missatgeria', 25),
                    array('UF4. Serveis d’audio I video', 24)
                )
            ),
            array('M09 - Implantació d’aplicacions Web (CFGS - ASIX)', 66,
                array(
                    array('UF1.Llenguatges de guions de servidor', 33),
                    array('UF2.Implantació de gestors de continguts', 33)
                )
            ),
            array('M10 - Administració de sistemes gestors de bases de dades (CFGS - ASIX)', 99,
                array(
                    array('UF1. Llenguatges SQL: DCL i extensió procedimental ', 66),
                    array('UF2. Instal•lació i ajustament de SGBD corporatiu', 33)
                )
            ),
            array('M11 - Seguretat i alta disponibilitat (CFGS - ASIX)', 99,
                array(
                    array('UF1. Seguretat física, lògica i legislació', 24),
                    array('UF2. Seguretat activa i accés remot', 24),
                    array('UF3. Tallafocs i servidors intermediaris', 27),
                    array('UF4. Alta disponibilitat', 24)
                )
            ),
        );
        foreach ($masixs as $masix){
            $modulo = Modulo::create([
                'nombre'    =>  $masix[0],
                'durada'    =>  $masix[1],
            ]);
            foreach ($masix[2] as $muf){
                $uf[] = UF::create([
                    'nombre'    => $muf[0],
                    'horas'    => $muf[1],
                ]);
            }
            $modulo->ufs()->saveMany($uf);
            $uf = [];
        }

        $msmxs = array(
            array('M01 - Muntatge i manteniment d’equips (CFGM - SMX)', 198,
                array(
                    array('UF 1: Electricitat a l’ordinador', 33),
                    array('UF 2: Components d’un equip microinformàtic', 66),
                    array('UF 5: Manteniment d’equips microinformàtics', 33),
                    array('UF 5: Manteniment d’equips microinformàtics', 33),
                    array('UF 6: Instal·lació de programari', 33)
                )
            ),
            array('M02 - Sistemes operatius monolloc (CFGM - SMX)', 132,
                array(
                    array('UF 1: Introducció als sistemes operatius', 33),
                    array('UF 2: Sistemes operatius propietaris', 66)
                )
            ),
            array('M03 - Aplicacions ofimàtiques (CFGM - SMX)', 165,
                array(
                    array('UF 1: Aplicacions ofimàtiques i atenció a l’usuari', 33),
                    array('UF 2: El correu i l’agenda electrònica', 66),
                    array('UF 3: Processadors de text', 33),
                    array('UF 4: Fulls de càlcul', 33),
                    array('UF 5: Bases de dades', 33),
                    array('UF 5: Bases de dades', 33),
                    array('UF 5: Bases de dades', 33),
                    array('UF 6: Imatge i vídeo – Presentacions', 33),
                )
            ),
            array('M04 - Sistemes operatius en xarxa (CFGM - SMX)', 132,
                array(
                    array('UF 1: Sistemes operatius propietaris en xarxa', 33),
                    array('UF 2: Sistemes operatius lliures en xarxa', 66),
                    array('UF 3: Compartició de recursos i seguretat', 33),
                    array('UF 4: Integració de sistemes operatius', 33)
                )
            ),
            array('M05 - Xarxes locals (CFGM - SMX)', 165,
                array(
                    array('UF 1: Introducció a les xarxes locals', 33),
                    array('UF 2: Configuració de commutadors i encaminadors', 66),
                    array('UF 3: Resolució d’incidències en xarxes locals', 33),
                )
            ),
            array('M06 - Seguretat informàtica (CFGM - SMX)', 132,
                array(
                    array('UF 1: Seguretat passiva', 33),
                    array('UF 2: Còpies de seguretat', 66),
                    array('UF 3: Legislació de seguretat i protecció de dades', 33),
                    array('UF 4: Seguretat activa', 33),
                    array('UF 5: Tallafocs i monitoratge de xarxes.', 33)
                )
            ),
            array('M07 - Serveis de xarxa (CFGM - SMX)', 165,
                array(
                    array('UF 1 : Configuració de la xarxa (DNS i DHCP)', 33),
                    array('UF 2: Correu electrònic i transmissió d’arxius', 66),
                    array('UF 3: Servidor web i proxy', 33),
                    array('UF 4: Accés a sistemes remots', 33),
                )
            ),
            array('M08 - Aplicacions web (CFGM - SMX)', 198,
                array(
                    array('UF 1: Ofimàtica i eines web', 33),
                    array('UF 2: Gestors d’arxius web', 66),
                    array('UF 3: Gestors de continguts', 33),
                    array('UF 4: Portals web d’aprenentatge', 33),
                    array('UF 5. Fonaments d\' HTML i fulls d\'estils', 33)
                )
            ),
        );

        foreach ($msmxs as $masix){
            $modulo = Modulo::create([
                'nombre'    =>  $masix[0],
                'durada'    =>  $masix[1],
            ]);
            foreach ($masix[2] as $muf){
                $uf[] = UF::create([
                    'nombre'    => $muf[0],
                    'horas'    => $muf[1],
                ]);
            }
            $modulo->ufs()->saveMany($uf);
            $uf = [];
        }
    }
}
