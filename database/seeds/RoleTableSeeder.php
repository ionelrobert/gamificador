<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'name' => 'superprofesor',
            'display_name' => 'Rol de Superprofesor',
            'description' => 'Rol del administrador del Gamificador',
        ]);
        Role::create([
            'name' => 'profe',
            'display_name' => 'Rol de Profesor',
            'description' => 'Rol del Profesor',
        ]);
        Role::create([
            'name' => 'alumno',
            'display_name' => 'Rol de Alumno',
            'description' => 'Rol del Alumno',
        ]);
    }
}
