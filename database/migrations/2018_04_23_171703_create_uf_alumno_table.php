<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUfAlumnoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uf_alumno', function (Blueprint $table) {
            $table->integer('alumno_id')->unsigned();
            $table->integer('uf_id')->unsigned();

            $table->foreign('alumno_id')->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('uf_id')->references('id')->on('ufs')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->primary(['alumno_id','uf_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uf_alumno');
    }
}
